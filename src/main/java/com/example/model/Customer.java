package com.example.model;

public class Customer {
	
	private Long customerId;
	private String customerName;
	private String loanNo;
	private String policy;
	private String policyStatus;
	
	public Customer(){
		
	}
	public Customer(long customerId,String customerName,String loanNo,String policy,String policyStatus){
		this.customerId = customerId;
		this.customerName = customerName;
		this.loanNo = loanNo;
		this.policy =policy;
		this.policyStatus = policyStatus;
	}
	
	public Customer(Long customerId){
		this.setCustomerId(customerId);
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	public String getPolicy() {
		return policy;
	}

	public void setPolicy(String policy) {
		this.policy = policy;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

}
