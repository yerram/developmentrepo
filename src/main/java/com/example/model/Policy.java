package com.example.model;

public class Policy {
	
	private Long policyNo;
	private String applicationType;
	private String status;
	private String paymentPlan;
	private String terminationType;
	private String LRV;
	private String terminationReason;
	private String coverage;
	private String customerNumber;
	private String customerName;
	
	public Policy() {
		
	}
	
	public Policy(Long policyNo,String applicationType ,String status ,String paymentPlan) {
		this.policyNo = policyNo;
		this.applicationType = applicationType;
		this.status = status;
		this.paymentPlan = paymentPlan;
	}

	public Long getPolicyNo() {
		return policyNo;
	}

	public void setPolicyno(Long policyNo) {
		this.policyNo = policyNo;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentPlan() {
		return paymentPlan;
	}

	public void setPaymentPlan(String paymentPlan) {
		this.paymentPlan = paymentPlan;
	}

	public String getTerminationType() {
		return terminationType;
	}

	public void setTerminationType(String terminationType) {
		this.terminationType = terminationType;
	}

	public String getLRV() {
		return LRV;
	}

	public void setLRV(String lRV) {
		LRV = lRV;
	}

	public String getTerminationReason() {
		return terminationReason;
	}

	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
		
}
