package com.example.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Policy;
import com.example.service.PolicyService;

@RestController
@RequestMapping("/api")
public class PolicyController {
	@Autowired
	PolicyService _policyService;

	@RequestMapping(value = "/policy/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Policy> findPolicyByNumber(@PathVariable("id") Long policyNumber) {
		Policy policy = _policyService.getPolicyByNumber(policyNumber);
		if(policy != null) {
			return new ResponseEntity<Policy>(policy, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Policy>(policy,HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/policy", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<Policy>> findAll() {
		List<Policy> policyList = new ArrayList<Policy>();
		policyList = _policyService.getAll();
		return new ResponseEntity<List<Policy>>(policyList, HttpStatus.OK);
	}
}
