package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.model.Customer;
import com.example.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerController {
	@Autowired
	private CustomerService _customerService;

	@RequestMapping(value = "/customer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Void> createCustomer(@RequestBody Customer customer,UriComponentsBuilder ucBuilder) {
		_customerService.save(customer);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/customer/{id}").buildAndExpand(customer.getCustomerId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Customer> findCustomerById(@PathVariable("id") Long customerId) {

		Customer customer = _customerService.getCustomerById(customerId);
		if (customer != null) {
			return new ResponseEntity<Customer>(customer, HttpStatus.OK);
		} else {
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/customer/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Customer> delete(@PathVariable("id") Long customerId) {
		Customer customer = _customerService.getCustomerById(customerId);
		if (customer == null) {
			System.out.println("Unable to delete. User with id " + customerId + " not found");
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		}
		_customerService.delete(customerId);
		return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
	}


	@RequestMapping(value = "/customer/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Customer> update(@PathVariable("id") long customerId ,@RequestBody Customer customer) {
		Customer currentCustomer = _customerService.getCustomerById(customerId);

		if (currentCustomer==null) {
			System.out.println("User with id " + customerId + " not found");
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		}
		currentCustomer.setCustomerName(customer.getCustomerName());
		currentCustomer.setLoanNo(customer.getLoanNo());
		currentCustomer.setPolicy(customer.getPolicy());
		currentCustomer.setPolicyStatus(customer.getPolicyStatus());
		_customerService.update(currentCustomer);
		return new ResponseEntity<Customer>(currentCustomer, HttpStatus.OK);
	}

	@RequestMapping(value = "/customer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Customer>> readData() {
		List<Customer> customers = _customerService.selectAll();
		if(customers.isEmpty()){
			return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
	}

}
