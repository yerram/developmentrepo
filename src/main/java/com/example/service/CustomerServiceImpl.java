package com.example.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.model.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final AtomicLong counter = new AtomicLong();
	
	private static List<Customer> customers;
    
    static{
    	customers= populateCustomers();
    }
    
    private static List<Customer> populateCustomers() {
		 List<Customer> customers = new ArrayList<Customer>();
	        customers.add(new Customer(counter.incrementAndGet(),"Customer1","12345","yes","Active"));
	        customers.add(new Customer(counter.incrementAndGet(),"Customer2","12345","yes","Active"));
	        customers.add(new Customer(counter.incrementAndGet(),"Customer3","12345","yes","Active"));
	        customers.add(new Customer(counter.incrementAndGet(),"Customer4","12345","yes","Active"));
	        return customers;
	}



	@Override
	public Customer getCustomerById(Long customerId) {
		for(Customer customer : customers){
            if(customer.getCustomerId() == customerId){
                return customer;
            }
        }
        return null;

	}

	
	@Override
	public void save(Customer customer) {
		 customer.setCustomerId(counter.incrementAndGet());
	        customers.add(customer);
	}

	@Override
	public void delete(Long customerId) {
		 for (Iterator<Customer> iterator = customers.iterator(); iterator.hasNext(); ) {
	            Customer customer = iterator.next();
	            if (customer.getCustomerId() == customerId) {
	                iterator.remove();
	            }
	        }
	}

	@Override
	public void update(Customer customer) {
		 int index = customers.indexOf(customer);
		 customers.set(index, customer);
	}

	@Override
	public List<Customer> selectAll() {
		 return customers;
	}


}
