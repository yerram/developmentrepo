package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import com.example.model.Policy;

@Service
public class PolicyServiceImpl implements PolicyService{
	
	private static List<Policy> policies;
	private static final AtomicLong counter = new AtomicLong();
	
	 static{
		 policies= populatePolicies();
	 }
	 
	private static List<Policy> populatePolicies() {
		 List<Policy> policies = new ArrayList<Policy>();
		 policies.add(new Policy(counter.incrementAndGet(),"New","InForce","Single"));
		 policies.add(new Policy(counter.incrementAndGet(),"New","InForce","Single"));
		 policies.add(new Policy(counter.incrementAndGet(),"New","InForce","Single"));
		 policies.add(new Policy(counter.incrementAndGet(),"New","InForce","Single"));
	        return policies;
	}
	
	@Override
	public Policy getPolicyByNumber(Long policyNo) {
		for(Policy policy : policies){
            if(policy.getPolicyNo() == policyNo){
                return policy;
            }
        }
        return null;
	}
	@Override
	public List<Policy> getAll() {
		return policies;
	}
}
