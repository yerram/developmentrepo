package com.example.service;

import java.util.List;

import com.example.model.Policy;

public interface PolicyService {
	
	Policy getPolicyByNumber(Long policyNo);
	public List<Policy> getAll();
}
