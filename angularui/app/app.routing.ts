import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard }      from './auth-guard.service';
import { LoginService }      from './login/login.service';
import { UserHomeComponent } from './user/home/user-home.component';
/*
    The approuting for specifiying the routes for the application.
*/
const appRoutes: Routes = [
    { path: '', redirectTo:'/login', pathMatch:'full'},
    { path: 'login', component: LoginComponent },
    { path:'userhome', canActivate: [AuthGuard], component: UserHomeComponent },
    { path:'**', redirectTo:'/login', pathMatch:'full'}
];

export const appRoutingProviders: any[] = [
AuthGuard, LoginService
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
