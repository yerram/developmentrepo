import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { LoginComponent } from './login/login.component';
import { UserHomeComponent } from './user/home/user-home.component';
import { routing, appRoutingProviders } from './app.routing';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { LoginService } from './login/login.service';
import { HttpModule} from '@angular/http';
import { UserModule } from './user/user.module';
import { CreatePartyComponent } from './user/createparty/create-party.component';
import { PartyBasicInformationComponent } from './user/createparty/partybasicinformation/party-basic-information.component';
import { PartyOptionalInformationComponent } from './user/createparty/partyoptionalinformation/party-optional-information.component';
import { PartyRoles } from './user/createparty/partyroles/party-roles.component';
import { Address } from './user/createparty/address/address.component';
import { CreatePartyService } from './user/createparty/create-party.service';
import {MaintainPartyComponent} from './user/maintainParty/maintain-party.component';
import {MaintainPartyService} from './user/maintainParty/maintain-party.service';
import { UpdatePartyService } from './user/shared/update-party.service';
import {InputTextModule,DataTableModule,ButtonModule,DialogModule, SharedModule} from 'primeng/primeng';
/*
    The appmodule is the root module.It loads all the classes.
*/
@NgModule({
  imports: [BrowserModule, routing, FormsModule, ReactiveFormsModule, HttpModule, UserModule,InputTextModule,DataTableModule,ButtonModule,DialogModule, SharedModule],
  declarations: [AppComponent, LoginComponent, UserHomeComponent, CreatePartyComponent, MaintainPartyComponent, PartyBasicInformationComponent, PartyOptionalInformationComponent, PartyRoles, Address],
  bootstrap: [AppComponent],
  providers: [appRoutingProviders, LoginService, CreatePartyService,MaintainPartyService, UpdatePartyService]
})
export class AppModule { }
