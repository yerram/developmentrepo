"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var app_component_1 = require('./app.component');
var login_component_1 = require('./login/login.component');
var user_home_component_1 = require('./user/home/user-home.component');
var app_routing_1 = require('./app.routing');
var forms_1 = require('@angular/forms');
var login_service_1 = require('./login/login.service');
var http_1 = require('@angular/http');
var user_module_1 = require('./user/user.module');
var create_party_component_1 = require('./user/createparty/create-party.component');
var party_basic_information_component_1 = require('./user/createparty/partybasicinformation/party-basic-information.component');
var party_optional_information_component_1 = require('./user/createparty/partyoptionalinformation/party-optional-information.component');
var party_roles_component_1 = require('./user/createparty/partyroles/party-roles.component');
var address_component_1 = require('./user/createparty/address/address.component');
var create_party_service_1 = require('./user/createparty/create-party.service');
var maintain_party_component_1 = require('./user/maintainParty/maintain-party.component');
var maintain_party_service_1 = require('./user/maintainParty/maintain-party.service');
var update_party_service_1 = require('./user/shared/update-party.service');
var primeng_1 = require('primeng/primeng');
/*
    The appmodule is the root module.It loads all the classes.
*/
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, app_routing_1.routing, forms_1.FormsModule, forms_1.ReactiveFormsModule, http_1.HttpModule, user_module_1.UserModule, primeng_1.InputTextModule, primeng_1.DataTableModule, primeng_1.ButtonModule, primeng_1.DialogModule, primeng_1.SharedModule],
            declarations: [app_component_1.AppComponent, login_component_1.LoginComponent, user_home_component_1.UserHomeComponent, create_party_component_1.CreatePartyComponent, maintain_party_component_1.MaintainPartyComponent, party_basic_information_component_1.PartyBasicInformationComponent, party_optional_information_component_1.PartyOptionalInformationComponent, party_roles_component_1.PartyRoles, address_component_1.Address],
            bootstrap: [app_component_1.AppComponent],
            providers: [app_routing_1.appRoutingProviders, login_service_1.LoginService, create_party_service_1.CreatePartyService, maintain_party_service_1.MaintainPartyService, update_party_service_1.UpdatePartyService]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map