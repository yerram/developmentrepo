import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';
/*
    main tells Angular to start the Application
*/
const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);
