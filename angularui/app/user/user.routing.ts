import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserHomeComponent } from './home/user-home.component';
import { AuthGuard }      from '../auth-guard.service';
import { CreatePartyComponent } from './createparty/create-party.component';
import {MaintainPartyComponent} from './maintainParty/maintain-party.component';
/*
    The userrouting for specifiying the routes which are under User.
*/
const userRoutes: Routes = [
    { path:'userhome', component: UserHomeComponent, canActivateChild: [AuthGuard], children:[
         { path:'createParty', component: CreatePartyComponent},
         { path: 'maintainParty', component: MaintainPartyComponent}
    ]},
   
];

export const userRoutingProviders: any[] = [

];

export const UserRouting: ModuleWithProviders = RouterModule.forChild(userRoutes);