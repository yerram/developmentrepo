import { Component, OnInit } from '@angular/core';
import myGlobals = require('../../global');
import {Router} from '@angular/router';
import { LoginService } from '../../login/login.service';
import { UpdatePartyService } from '../shared/update-party.service';
@Component({
    templateUrl: '/app/user/home/user-home.component.html'
})
export class UserHomeComponent {
    public loggedUser: string = myGlobals.usr;
    constructor(private router: Router, private loginService: LoginService, private updatePartyService: UpdatePartyService) {
    }
    signoutClicked(){
        this.loginService.logout();
        this.router.navigate(['/login']);
    }

  creatParty(){
      this.updatePartyService.clearParty();
      this.router.navigate(['userhome/createParty']);
  }
  maintainParty(){
      this.updatePartyService.clearParty();
      this.router.navigate(['userhome/maintainParty']);
  }
}