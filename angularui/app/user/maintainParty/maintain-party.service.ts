import { Injectable } from '@angular/core';
import { Http, HttpModule, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
/*
    The maintain-party service calls the backend webservice 
    to fetch all the parties.
*/
@Injectable()
export class MaintainPartyService {
    //https://api.myjson.com/bins/5c2ok
    //http://MNGNET309744D:8080/api/party 
    private partyUrl = 'https://api.myjson.com/bins/5c2ok';  // URL to web API
    constructor (private http: Http) {}

maintainPartyDetails(){
    return this.http.get(this.partyUrl)
                    .map(this.extractData)
                    .catch(this.handleError);
  }
  private extractData(res: Response) {
    let body = res.json();
console.log(body);
    return body || { };
  }
 private handleError(error: any) {
        return Observable.throw("Error");
    }
}
    