import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MaintainPartyService} from './maintain-party.service';
import { Pipe, PipeTransform } from '@angular/core';
import {Party} from '../createparty/models/party.model';
import { UpdatePartyService } from '../shared/update-party.service';
import { DataTableModule, SharedModule} from 'primeng/primeng';
/*
    The maintain-partycomponent shows existing party details in 
    tabular form using maintain-party.service.
    Uses maintainParty.html as template.
*/
@Component({
    templateUrl: `/app/user/maintainParty/maintain-party.html`
})
export class MaintainPartyComponent implements OnInit {
    parties: Party[];
    constructor(private router: Router, private maintainPartyService: MaintainPartyService, private updatePartyService: UpdatePartyService) {

    }
    
    errorMessage: string;
    ngOnInit(): void {
        this.maintainPartyService.maintainPartyDetails()
            .subscribe(
            parties => this.parties = parties,
            error => this.errorMessage = <any>error);

    }

    onRowSelect(event): void {
        this.updatePartyService.sendParty(event.data);
        this.router.navigate(['userhome/createParty']);
    }

    buttonValue:string="Clear";
    clear:boolean=false;
     clearFunction(): void {
        if(!this.clear){
            this.clear=true;
            this.buttonValue="Filter";
        }
        else{
             this.clear=false;
             this.buttonValue="Clear";
        }
    }
}