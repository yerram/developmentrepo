"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var maintain_party_service_1 = require('./maintain-party.service');
var update_party_service_1 = require('../shared/update-party.service');
/*
    The maintain-partycomponent shows existing party details in
    tabular form using maintain-party.service.
    Uses maintainParty.html as template.
*/
var MaintainPartyComponent = (function () {
    function MaintainPartyComponent(router, maintainPartyService, updatePartyService) {
        this.router = router;
        this.maintainPartyService = maintainPartyService;
        this.updatePartyService = updatePartyService;
        this.buttonValue = "Clear";
        this.clear = false;
    }
    MaintainPartyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.maintainPartyService.maintainPartyDetails()
            .subscribe(function (parties) { return _this.parties = parties; }, function (error) { return _this.errorMessage = error; });
    };
    MaintainPartyComponent.prototype.onRowSelect = function (event) {
        this.updatePartyService.sendParty(event.data);
        this.router.navigate(['userhome/createParty']);
    };
    MaintainPartyComponent.prototype.clearFunction = function () {
        if (!this.clear) {
            this.clear = true;
            this.buttonValue = "Filter";
        }
        else {
            this.clear = false;
            this.buttonValue = "Clear";
        }
    };
    MaintainPartyComponent = __decorate([
        core_1.Component({
            templateUrl: "/app/user/maintainParty/maintain-party.html"
        }), 
        __metadata('design:paramtypes', [router_1.Router, maintain_party_service_1.MaintainPartyService, update_party_service_1.UpdatePartyService])
    ], MaintainPartyComponent);
    return MaintainPartyComponent;
}());
exports.MaintainPartyComponent = MaintainPartyComponent;
//# sourceMappingURL=maintain-party.component.js.map