"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var party_model_1 = require('../createparty/models/party.model');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/observable/of');
/*
    The updateparty service to send Party model from
    maintain-party to createpartycomponent to view Party details
    for updating.
*/
var UpdatePartyService = (function () {
    function UpdatePartyService() {
        this.party = new party_model_1.Party();
    }
    UpdatePartyService.prototype.sendParty = function (party) {
        this.party = party;
    };
    UpdatePartyService.prototype.getParty = function () {
        return Observable_1.Observable.of(this.party);
    };
    UpdatePartyService.prototype.clearParty = function () {
        this.party = new party_model_1.Party();
    };
    UpdatePartyService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], UpdatePartyService);
    return UpdatePartyService;
}());
exports.UpdatePartyService = UpdatePartyService;
//# sourceMappingURL=update-party.service.js.map