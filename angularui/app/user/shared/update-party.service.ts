import { Injectable } from '@angular/core';
import { Party } from '../createparty/models/party.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
/*
    The updateparty service to send Party model from
    maintain-party to createpartycomponent to view Party details
    for updating.
*/

@Injectable()
export class UpdatePartyService {
    party:Party=new Party();
    sendParty(party:Party):void{
        this.party=party;
    }
    getParty():Observable<Party>{
        return Observable.of(this.party);
    }
    clearParty():void{
        this.party=new Party();
    }
}