import { PartyBasicInforamtion } from './party-basic-information.model';
import { PartyOptionalInformation } from './party-optional-information.model';
import { PartyRoles } from './party-roles.model';
import { Address } from './address.model'
/*
    The party.model used for Party details.
    Uses PartyBasicInforamtion, PartyOptionalInformation,
    PartyRoles and Address models.
*/
    
export class Party{
    partyBasicInformation:PartyBasicInforamtion= new PartyBasicInforamtion();
    partyOptionalInformation:PartyOptionalInformation = new PartyOptionalInformation();
    partyRoles:PartyRoles= new PartyRoles();
    address: Address= new Address();
}