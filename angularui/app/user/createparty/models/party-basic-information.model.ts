/*
    The partybasicinformation model used for PartyBasicInformation details.
*/
export class PartyBasicInforamtion{
    partyId:number;
    partyName:string;
    nickName:string;
    partyStatus:string;
    partyType:string;
    constructor(){
        this.partyType="";
        this.partyStatus="Pending";
    }
}