/*
    The partyoptionalinformation model used for PartyOptionalInformation details.
*/
export class PartyOptionalInformation{
    partyRefId:string;
    archLMIRefId:string;
}