"use strict";
var party_basic_information_model_1 = require('./party-basic-information.model');
var party_optional_information_model_1 = require('./party-optional-information.model');
var party_roles_model_1 = require('./party-roles.model');
var address_model_1 = require('./address.model');
/*
    The party.model used for Party details.
    Uses PartyBasicInforamtion, PartyOptionalInformation,
    PartyRoles and Address models.
*/
var Party = (function () {
    function Party() {
        this.partyBasicInformation = new party_basic_information_model_1.PartyBasicInforamtion();
        this.partyOptionalInformation = new party_optional_information_model_1.PartyOptionalInformation();
        this.partyRoles = new party_roles_model_1.PartyRoles();
        this.address = new address_model_1.Address();
    }
    return Party;
}());
exports.Party = Party;
//# sourceMappingURL=party.model.js.map