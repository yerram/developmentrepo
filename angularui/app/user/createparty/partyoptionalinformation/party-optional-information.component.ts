import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
/*
    The partyoptionalinformationcomponent provides input fields for 
    optional details.
    Uses partyoptionalinformation.component.html as template.
*/
@Component({
    selector: 'partyoptionalinformation',
    templateUrl: '/app/user/createparty/partyoptionalinformation/party-optional-information.component.html'
})
export class PartyOptionalInformationComponent {
    @Input('optionalinfogroup') partyOptionalInformationForm: FormGroup;
}