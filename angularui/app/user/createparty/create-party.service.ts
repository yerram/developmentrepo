import { Injectable } from '@angular/core';
import { Http,Headers, RequestOptions } from '@angular/http';
import { Party } from './models/party.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
/*
    The createpartyservice calls the backend webservice 
    and creates or updates party with the details provided .
    Throws exception if not successfull.
*/
@Injectable()
export class CreatePartyService {
     message: string;
    constructor(private http:Http) { }

    createParty(party:Party){
        let body=JSON.stringify(party);
        console.log(party.partyBasicInformation.partyId);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        if(party.partyBasicInformation.partyId){
            return this.http.put('http://MNGNET309744D:8080/api/party/'+party.partyBasicInformation.partyId, body, options)
            .map(data => {
                this.message=data.text();
                return data.ok;
            })
            .catch(this.handleErrorUpdate);
        }
        else{
            return this.http.post('http://MNGNET309744D:8080/api/party', body, options)
            .map(data => {
                this.message=data.text();
                return data.ok;
            })
            .catch(this.handleErrorCreate);
        }
    }
    private handleErrorCreate(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better messag
        return Observable.throw("Party Creation Unsuccessfull");
    }
    private handleErrorUpdate(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better messag
        return Observable.throw("Party Updation Unsuccessfull");
    }
}