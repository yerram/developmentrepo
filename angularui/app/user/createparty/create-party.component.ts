import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Party } from './models/party.model';
import { CreatePartyService } from './create-party.service';
import { UpdatePartyService } from '../shared/update-party.service';

/*
    The createparty Component allows user to create a new party
    and also update and existing party details.Calls createparty.service
    for creating a new party and updateparty.service for updating 
    party details.
    Uses FormBuilder for including different forms, different forms
    used: partybasicinformation, partyoptionalinformation,address and
    partyoptionalinformation.
    Uses createparty.component.html as template.
*/

@Component({
    templateUrl: '/app/user/createparty/create-party.component.html',
    styleUrls:['app/user/createparty/create-party.component.css']
})
export class CreatePartyComponent implements OnInit {
    public partyForm: FormGroup;
    private message: string;
    private styleClass:string;
    party: Party = new Party();
    constructor(private _fb: FormBuilder, private createPartyService: CreatePartyService, private updatePartyService: UpdatePartyService) { }

    ngOnInit() {
        this.updatePartyService.getParty().subscribe(
            data=>{
                this.party=data;
            }
        )
        this.partyForm= this._fb.group({
            partyBasicInformation: this._fb.group({
                partyId:[this.party.partyBasicInformation.partyId],
                partyName:[this.party.partyBasicInformation.partyName,[Validators.required, Validators.maxLength(50), Validators.pattern('[0-9A-Za-z]*')]],
                nickName:[this.party.partyBasicInformation.nickName,[Validators.required, Validators.maxLength(10), Validators.pattern('[0-9A-Za-z]*')]],
                partyStatus:[{value:this.party.partyBasicInformation.partyStatus, disabled:this.party.partyBasicInformation.partyId?false:true}],
                partyType:[this.party.partyBasicInformation.partyType,[Validators.required]]
            }),
            partyOptionalInformation: this._fb.group({
                partyRefId:[this.party.partyOptionalInformation.partyRefId, [Validators.maxLength(50), Validators.pattern('[0-9A-Za-z]*')]],
                archLMIRefId:[this.party.partyOptionalInformation.archLMIRefId, [Validators.maxLength(50), Validators.pattern('[0-9A-Za-z]*')]]
            }),
            address: this._fb.group({
                type:[this.party.address.type],
                attn:[this.party.address.attn, [Validators.maxLength(150), Validators.pattern('[0-9A-Za-z]*')]],
                addressLine1:[this.party.address.addressLine1, [Validators.maxLength(150), Validators.pattern('[0-9A-Za-z]*')]],
                addressLine2:[this.party.address.addressLine2, [Validators.maxLength(150), Validators.pattern('[0-9A-Za-z]*')]],
                city:[this.party.address.city, [Validators.maxLength(50), Validators.pattern('[0-9A-Za-z]*')]],
                state:[this.party.address.state],
                postCode:[this.party.address.postCode, [Validators.pattern('[0-9]{7}')]],
                country:[this.party.address.country]
            }),
            partyRoles:this._fb.group({
                partyRoles:[this.party.partyRoles.partyRoles]
            })
        })
     }
     save(party: any){
         
         this.createPartyService.createParty(party.value).subscribe(
            data => {
                if (data) {
                    this.message=this.createPartyService.message;
                    this.styleClass="text-success";
                }
            },
            error=>{
                this.message=<any>error;
                this.styleClass="text-danger";
            }
         );
     }
}