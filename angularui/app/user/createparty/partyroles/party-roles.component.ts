import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
/*
    The partyrolescomponent provides input fields for 
    optional details.
    Uses partyroles.component.html as template.
*/
@Component({
    selector: 'partyroles',
    templateUrl: '/app/user/createparty/partyroles/party-roles.component.html'
})
export class PartyRoles {
    @Input('rolesgroup') partyRolesForm: FormGroup;
    roles: string[]=['Originator','Servicer','Reinsurer','Parent Party'];
    checkedRoles:string[]= [];
    
    checkboxes(role){
        this.checkedRoles=this.partyRolesForm.get('partyRoles').value;
        if(this.checkedRoles.indexOf(role)>=0){

            this.checkedRoles.splice(this.checkedRoles.indexOf(role),1);
        }
        else{
            this.checkedRoles.push(role);
        }
        if(this.checkedRoles.indexOf("Originator")>=0 && this.checkedRoles.length==1)
        {
            this.checkedRoles.push('Servicer');
        }
        this.partyRolesForm.setValue({partyRoles:this.checkedRoles});
    }
    findRole(role): boolean{
        if(this.partyRolesForm.get('partyRoles').value.indexOf(role)>=0){
            return true;
        }
        return false;
    }
}