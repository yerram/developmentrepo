import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
/*
    The partybasicinformationcomponent provides input fields for 
    basic details.
    Uses partybasicinformation.component.html as template.
*/
@Component({
    selector: 'partybasicinformation',
    templateUrl: '/app/user/createparty/partybasicinformation/party-basic-information.component.html'
})
export class PartyBasicInformationComponent implements OnInit {
    @Input('basicinfogroup') partyBasicInformationForm: FormGroup;
    partyId:number;
    partyTypes:string[]=["Building Societies","Credit Unions","Large Banks","Non Bank Lenders","Regional Banks"];
    partySt:string[]=["Active","Expired","Inactive","Pending"];
    ngOnInit(){
        
        if(this.partyBasicInformationForm.get('partyId')){
            this.partyId=this.partyBasicInformationForm.get('partyId').value;
        }
    
}
}

