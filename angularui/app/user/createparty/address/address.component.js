"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
/*
    The address.component provides input fields for
    address details.
    Uses address.component.html as template.
*/
var Address = (function () {
    function Address() {
        this.states = ["Australian Capital Territory", "New South Wales", "Northern Territory", "Queensland", "South Australia", "Tasmania", "Victoria", "Western Australia"];
    }
    __decorate([
        core_1.Input('addressgroup'), 
        __metadata('design:type', forms_1.FormGroup)
    ], Address.prototype, "addressForm", void 0);
    Address = __decorate([
        core_1.Component({
            selector: 'address',
            templateUrl: '/app/user/createparty/address/address.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], Address);
    return Address;
}());
exports.Address = Address;
//# sourceMappingURL=address.component.js.map