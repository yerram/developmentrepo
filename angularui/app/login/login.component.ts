import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import myGlobals = require('../global');
import { LoginService } from './login.service';
import { User } from '../user/user.model';
/*
    The login.component Validates the user. It calls the
    login service and based on the response only navigates
    the application to Home Page 
    Uses login.component.html as template.
*/
@Component({
    selector: 'selector',
    templateUrl: '/app/login/login.component.html'
})
export class LoginComponent {
    private user: User = new User();
    private errorMessage:string;
    constructor(private router: Router, private loginService: LoginService) {
        this.errorMessage=null;
     }
    loginClicked(event) {
        this.loginService.login(this.user).subscribe(
            data => {
                this.errorMessage=null;
                if (data) {
                    myGlobals.usr=this.user.user_name;
                    let redirect ='/userhome';
                    // Redirect the user
                    this.router.navigate([redirect]);
                }
            },
            error=>{
                this.errorMessage=error;
            }
        )

    }
}