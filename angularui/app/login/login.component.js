"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var myGlobals = require('../global');
var login_service_1 = require('./login.service');
var user_model_1 = require('../user/user.model');
/*
    The login.component Validates the user. It calls the
    login service and based on the response only navigates
    the application to Home Page
    Uses login.component.html as template.
*/
var LoginComponent = (function () {
    function LoginComponent(router, loginService) {
        this.router = router;
        this.loginService = loginService;
        this.user = new user_model_1.User();
        this.errorMessage = null;
    }
    LoginComponent.prototype.loginClicked = function (event) {
        var _this = this;
        this.loginService.login(this.user).subscribe(function (data) {
            _this.errorMessage = null;
            if (data) {
                myGlobals.usr = _this.user.user_name;
                var redirect = '/userhome';
                // Redirect the user
                _this.router.navigate([redirect]);
            }
        }, function (error) {
            _this.errorMessage = error;
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'selector',
            templateUrl: '/app/login/login.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, login_service_1.LoginService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map