import { Injectable } from '@angular/core';
import { User } from '../user/user.model';
import myGlobals = require('../global');
import { Http, HttpModule, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

/*
    The login.service calls the backend webservice 
    and validates user .Returns true if a valid user.
*/
@Injectable()
export class LoginService {
    constructor(private http: Http) { }
    isLoggedIn: boolean = false;
    redirectUrl: string;
    login(user: User): Observable<boolean> {
     /*  let body = JSON.stringify(user);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://MNGNET309744D:8080/login', body, options)
            .map(data => {
                this.isLoggedIn = data.ok;
                return data.ok;
            })
            .catch(this.handleError);  to avoid service call*/    
       let body = JSON.stringify(user);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        return Observable.of(true).map(data=>this.isLoggedIn=true);
    }
    private handleError(error: any) {
        return Observable.throw("Invalid UserName or Password"); 
    }
    logout(): void {
        this.isLoggedIn = false;
        myGlobals.usr=null;
        this.redirectUrl=null;
    }

}