"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var myGlobals = require('../global');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/observable/throw');
require('rxjs/add/observable/of');
/*
    The login.service calls the backend webservice
    and validates user .Returns true if a valid user.
*/
var LoginService = (function () {
    function LoginService(http) {
        this.http = http;
        this.isLoggedIn = false;
    }
    LoginService.prototype.login = function (user) {
        var _this = this;
        /*  let body = JSON.stringify(user);
           var headers = new Headers();
           headers.append('Content-Type', 'application/json');
           let options = new RequestOptions({ headers: headers });
           return this.http.post('http://MNGNET309744D:8080/login', body, options)
               .map(data => {
                   this.isLoggedIn = data.ok;
                   return data.ok;
               })
               .catch(this.handleError);  to avoid service call*/
        var body = JSON.stringify(user);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        return Observable_1.Observable.of(true).map(function (data) { return _this.isLoggedIn = true; });
    };
    LoginService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw("Invalid UserName or Password");
    };
    LoginService.prototype.logout = function () {
        this.isLoggedIn = false;
        myGlobals.usr = null;
        this.redirectUrl = null;
    };
    LoginService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
//# sourceMappingURL=login.service.js.map