import { Component } from '@angular/core';

/*
    The app component is the root component for the application.
*/

@Component({
    selector: 'my-app',
    template: `
    <div class="container">
        <router-outlet></router-outlet>
    </div>
        `
})
export class AppComponent { }
