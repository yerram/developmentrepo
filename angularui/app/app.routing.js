"use strict";
var router_1 = require('@angular/router');
var login_component_1 = require('./login/login.component');
var auth_guard_service_1 = require('./auth-guard.service');
var login_service_1 = require('./login/login.service');
var user_home_component_1 = require('./user/home/user-home.component');
/*
    The approuting for specifiying the routes for the application.
*/
var appRoutes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'userhome', canActivate: [auth_guard_service_1.AuthGuard], component: user_home_component_1.UserHomeComponent },
    { path: '**', redirectTo: '/login', pathMatch: 'full' }
];
exports.appRoutingProviders = [
    auth_guard_service_1.AuthGuard, login_service_1.LoginService
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map