package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Customer;
import com.example.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerController {
	@Autowired
	private CustomerService _customerService;

	@RequestMapping(value = "/customer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Void> createCustomer(@RequestBody Customer customer) {
		_customerService.save(customer);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}



	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> findCustomerById(@PathVariable("id") Long customerId) {

		Customer customer = _customerService.getCustomerById(customerId);
		if (customer != null) {
			return new ResponseEntity<Object>(customer, HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Customer with id "+customerId+" is not available",HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/customer/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long customerId) {
		Customer customer = _customerService.getCustomerById(customerId);
		_customerService.delete(customerId);
		if (customer == null) {
			return new ResponseEntity<Object>("Unable to delete. Customer with id " + customerId + " not found",HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>("Deleted the customer with id"+customerId+"successfully.",HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/customer/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Object> update(@PathVariable("id") long customerId,@RequestBody Customer customer) {
		Customer currentCustomer = _customerService.getCustomerById(customerId);
		if (customer == null) {
			return new ResponseEntity<Object>("Unable to update. Customer with id " + customerId + " not found",HttpStatus.NOT_FOUND);
		}
		currentCustomer.setCustomerName(customer.getCustomerName());
		currentCustomer.setCustomerNickname(customer.getCustomerNickname());
		currentCustomer.setCustomerStatus(customer.getCustomerStatus());
		currentCustomer.setCustomerType(customer.getCustomerType());
		currentCustomer.setCustomerReferenceId(customer.getCustomerReferenceId());
		currentCustomer.setCustomerState(customer.getCustomerState());
		currentCustomer.setCustomerCity(customer.getCustomerCity());
		_customerService.update(currentCustomer);
		return new ResponseEntity<Object>(currentCustomer, HttpStatus.OK);
	}


	@RequestMapping(value = "/customer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Customer>> readData(Model model){
		List<Customer> customerList = _customerService.selectAll();
		return new ResponseEntity<List<Customer>>(customerList, HttpStatus.OK);
	}

}
