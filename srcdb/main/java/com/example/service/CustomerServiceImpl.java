package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.CustomerDao;
import com.example.model.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao _customerDao;

	@Override
	public Customer getCustomerById(Long customerId) {
		return _customerDao.findOne(customerId);

	}

	@Override
	public void save(Customer customer) {
		_customerDao.save(customer);
	}

	@Override
	public void delete(Long customerId) {
		_customerDao.delete(customerId);
	}

	@Override
	public Customer update(Customer customer) {
		return _customerDao.save(customer);
	}

	@Override
	public List<Customer> selectAll() {
		return _customerDao.findAll();
	}

}
