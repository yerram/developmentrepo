package com.example.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Customer;

@Transactional
public interface CustomerDao extends JpaRepository<Customer, Long> {
}