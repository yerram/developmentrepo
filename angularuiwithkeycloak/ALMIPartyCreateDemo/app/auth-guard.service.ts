import { Injectable }       from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
}                           from '@angular/router';
import { KeycloakService }      from './keycloak.service';

/*
    The auth-guard service is used to provide authentication
    against unauthorized access accross different components.
*/

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private keycloakService: KeycloakService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;

    return this.checkLogin(url);
  }
  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state) && KeycloakService.isAdmin;
  }

  checkLogin(url: string): boolean {
    if (KeycloakService.auth.loggedIn) { return true; }
    return false;
  }
}
