"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var myGlobals = require('../../global');
var router_1 = require('@angular/router');
var keycloak_service_1 = require('./../../keycloak.service');
var update_party_service_1 = require('../shared/update-party.service');
var UserHomeComponent = (function () {
    function UserHomeComponent(router, keycloakService, updatePartyService) {
        this.router = router;
        this.keycloakService = keycloakService;
        this.updatePartyService = updatePartyService;
        this.loggedUser = myGlobals.usr;
        this.isAdmin = keycloak_service_1.KeycloakService.isAdmin;
    }
    UserHomeComponent.prototype.signoutClicked = function () {
        this.keycloakService.logout();
    };
    UserHomeComponent.prototype.creatParty = function () {
        this.updatePartyService.clearParty();
        this.router.navigate(['userhome/createParty']);
    };
    UserHomeComponent.prototype.maintainParty = function () {
        this.updatePartyService.clearParty();
        this.router.navigate(['userhome/maintainParty']);
    };
    UserHomeComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/user/home/user-home.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, keycloak_service_1.KeycloakService, update_party_service_1.UpdatePartyService])
    ], UserHomeComponent);
    return UserHomeComponent;
}());
exports.UserHomeComponent = UserHomeComponent;
//# sourceMappingURL=user-home.component.js.map