import { Component, OnInit } from '@angular/core';
import myGlobals = require('../../global');
import {Router} from '@angular/router';
import { KeycloakService } from './../../keycloak.service';
import { UpdatePartyService } from '../shared/update-party.service';
@Component({
    templateUrl: '/app/user/home/user-home.component.html'
})
export class UserHomeComponent {
    public loggedUser: string = myGlobals.usr;
    isAdmin:boolean=KeycloakService.isAdmin;
    constructor(private router: Router, private keycloakService: KeycloakService, private updatePartyService: UpdatePartyService) {
    }
    signoutClicked(){
        this.keycloakService.logout();
    }

  creatParty(){
      this.updatePartyService.clearParty();
      this.router.navigate(['userhome/createParty']);
  }
  maintainParty(){
      this.updatePartyService.clearParty();
      this.router.navigate(['userhome/maintainParty']);
  }
}