"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var party_model_1 = require('./models/party.model');
var create_party_service_1 = require('./create-party.service');
var update_party_service_1 = require('../shared/update-party.service');
require('rxjs/add/operator/do');
/*
    The createparty Component allows user to create a new party
    and also update and existing party details.Calls createparty.service
    for creating a new party and updateparty.service for updating
    party details.
    Uses FormBuilder for including different forms, different forms
    used: partybasicinformation, partyoptionalinformation,address and
    partyoptionalinformation.
    Uses createparty.component.html as template.
*/
var CreatePartyComponent = (function () {
    function CreatePartyComponent(_fb, createPartyService, updatePartyService) {
        var _this = this;
        this._fb = _fb;
        this.createPartyService = createPartyService;
        this.updatePartyService = updatePartyService;
        this.party = new party_model_1.Party();
        this.updatePartyService.getParty().subscribe(function (data) {
            _this.party = data;
            _this.initFormGroup();
        });
    }
    CreatePartyComponent.prototype.ngOnInit = function () {
        this.initFormGroup();
    };
    CreatePartyComponent.prototype.initFormGroup = function () {
        this.partyForm = this._fb.group({
            partyBasicInformation: this._fb.group({
                partyId: [this.party.partyBasicInformation.partyId],
                partyName: [this.party.partyBasicInformation.partyName, [forms_1.Validators.required, forms_1.Validators.maxLength(50), forms_1.Validators.pattern('[0-9A-Za-z ]*')]],
                nickName: [this.party.partyBasicInformation.nickName, [forms_1.Validators.required, forms_1.Validators.maxLength(10), forms_1.Validators.pattern('[0-9A-Za-z ]*')]],
                partyStatus: [{ value: this.party.partyBasicInformation.partyStatus, disabled: this.party.partyBasicInformation.partyId ? false : true }],
                partyType: [this.party.partyBasicInformation.partyType, [forms_1.Validators.required]]
            }),
            partyOptionalInformation: this._fb.group({
                partyRefId: [this.party.partyOptionalInformation.partyRefId, [forms_1.Validators.maxLength(50), forms_1.Validators.pattern('[0-9A-Za-z]*')]],
                archLMIRefId: [this.party.partyOptionalInformation.archLMIRefId, [forms_1.Validators.maxLength(50), forms_1.Validators.pattern('[0-9A-Za-z]*')]]
            }),
            address: this._fb.group({
                type: [this.party.address.type],
                attn: [this.party.address.attn, [forms_1.Validators.maxLength(150), forms_1.Validators.pattern('[0-9A-Za-z ]*')]],
                addressLine1: [this.party.address.addressLine1, [forms_1.Validators.maxLength(150), forms_1.Validators.pattern('[0-9A-Za-z ]*')]],
                addressLine2: [this.party.address.addressLine2, [forms_1.Validators.maxLength(150), forms_1.Validators.pattern('[0-9A-Za-z ]*')]],
                city: [this.party.address.city, [forms_1.Validators.maxLength(50), forms_1.Validators.pattern('[0-9A-Za-z ]*')]],
                state: [this.party.address.state],
                postCode: [this.party.address.postCode, [forms_1.Validators.pattern('[0-9]{7}')]],
                country: [this.party.address.country]
            }),
            partyRoles: this._fb.group({
                partyRoles: [this.party.partyRoles.partyRoles]
            })
        });
    };
    CreatePartyComponent.prototype.save = function (party) {
        var _this = this;
        this.createPartyService.createParty(party.value).subscribe(function (data) {
            if (data) {
                _this.message = _this.createPartyService.message;
                _this.styleClass = "text-success";
            }
        }, function (error) {
            _this.message = error;
            _this.styleClass = "text-danger";
        });
    };
    CreatePartyComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/user/createparty/create-party.component.html',
            styleUrls: ['app/user/createparty/create-party.component.css']
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, create_party_service_1.CreatePartyService, update_party_service_1.UpdatePartyService])
    ], CreatePartyComponent);
    return CreatePartyComponent;
}());
exports.CreatePartyComponent = CreatePartyComponent;
//# sourceMappingURL=create-party.component.js.map