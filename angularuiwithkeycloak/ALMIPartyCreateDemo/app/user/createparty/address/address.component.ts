import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

/*
    The address.component provides input fields for 
    address details.
    Uses address.component.html as template.
*/
@Component({
    selector: 'address',
    templateUrl: '/app/user/createparty/address/address.component.html'
})
export class Address {
    @Input('addressgroup') addressForm: FormGroup;
    states: string[]=["Australian Capital Territory","New South Wales","Northern Territory","Queensland","South Australia","Tasmania","Victoria","Western Australia"];
}