"use strict";
/*
    The address.model used for address details.
*/
var Address = (function () {
    function Address() {
        this.type = "Physical";
        this.state = "";
        this.country = "Australia";
    }
    return Address;
}());
exports.Address = Address;
//# sourceMappingURL=address.model.js.map