"use strict";
/*
    The partyroles model used for PartyRoles details.
*/
var PartyRoles = (function () {
    function PartyRoles() {
        this.partyRoles = [];
    }
    return PartyRoles;
}());
exports.PartyRoles = PartyRoles;
//# sourceMappingURL=party-roles.model.js.map