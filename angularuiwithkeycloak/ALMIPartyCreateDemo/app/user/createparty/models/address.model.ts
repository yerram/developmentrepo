/*
    The address.model used for address details.
*/
export class Address{
	type:string="Physical";
    attn:string;
    addressLine1:string;
	addressLine2:string;
	city:string;
	state:string="";
	postCode:number;
	country:string="Australia";
}