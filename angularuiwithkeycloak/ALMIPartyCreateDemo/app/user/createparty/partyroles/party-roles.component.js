"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
/*
    The partyrolescomponent provides input fields for
    optional details.
    Uses partyroles.component.html as template.
*/
var PartyRoles = (function () {
    function PartyRoles() {
        this.roles = ['Originator', 'Servicer', 'Reinsurer', 'Parent Party'];
        this.checkedRoles = [];
    }
    PartyRoles.prototype.checkboxes = function (role) {
        this.checkedRoles = this.partyRolesForm.get('partyRoles').value;
        if (this.checkedRoles.indexOf(role) >= 0) {
            this.checkedRoles.splice(this.checkedRoles.indexOf(role), 1);
        }
        else {
            this.checkedRoles.push(role);
        }
        if (this.checkedRoles.indexOf("Originator") >= 0 && this.checkedRoles.length == 1) {
            this.checkedRoles.push('Servicer');
        }
        this.partyRolesForm.setValue({ partyRoles: this.checkedRoles });
    };
    PartyRoles.prototype.findRole = function (role) {
        if (this.partyRolesForm.get('partyRoles').value.indexOf(role) >= 0) {
            return true;
        }
        return false;
    };
    __decorate([
        core_1.Input('rolesgroup'), 
        __metadata('design:type', forms_1.FormGroup)
    ], PartyRoles.prototype, "partyRolesForm", void 0);
    PartyRoles = __decorate([
        core_1.Component({
            selector: 'partyroles',
            templateUrl: '/app/user/createparty/partyroles/party-roles.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], PartyRoles);
    return PartyRoles;
}());
exports.PartyRoles = PartyRoles;
//# sourceMappingURL=party-roles.component.js.map