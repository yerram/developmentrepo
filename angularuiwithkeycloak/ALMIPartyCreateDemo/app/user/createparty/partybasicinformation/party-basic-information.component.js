"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
/*
    The partybasicinformationcomponent provides input fields for
    basic details.
    Uses partybasicinformation.component.html as template.
*/
var PartyBasicInformationComponent = (function () {
    function PartyBasicInformationComponent() {
        this.partyTypes = ["Building Societies", "Credit Unions", "Large Banks", "Non Bank Lenders", "Regional Banks"];
        this.partySt = ["Active", "Expired", "Inactive", "Pending"];
    }
    PartyBasicInformationComponent.prototype.ngOnInit = function () {
        if (this.partyBasicInformationForm.get('partyId')) {
            this.partyId = this.partyBasicInformationForm.get('partyId').value;
        }
    };
    __decorate([
        core_1.Input('basicinfogroup'), 
        __metadata('design:type', forms_1.FormGroup)
    ], PartyBasicInformationComponent.prototype, "partyBasicInformationForm", void 0);
    PartyBasicInformationComponent = __decorate([
        core_1.Component({
            selector: 'partybasicinformation',
            templateUrl: '/app/user/createparty/partybasicinformation/partybasicinformation.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], PartyBasicInformationComponent);
    return PartyBasicInformationComponent;
}());
exports.PartyBasicInformationComponent = PartyBasicInformationComponent;
//# sourceMappingURL=party-basic-information.component.js.map