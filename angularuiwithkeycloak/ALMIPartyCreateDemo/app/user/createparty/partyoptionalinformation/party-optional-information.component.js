"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
/*
    The partyoptionalinformationcomponent provides input fields for
    optional details.
    Uses partyoptionalinformation.component.html as template.
*/
var PartyOptionalInformationComponent = (function () {
    function PartyOptionalInformationComponent() {
    }
    __decorate([
        core_1.Input('optionalinfogroup'), 
        __metadata('design:type', forms_1.FormGroup)
    ], PartyOptionalInformationComponent.prototype, "partyOptionalInformationForm", void 0);
    PartyOptionalInformationComponent = __decorate([
        core_1.Component({
            selector: 'partyoptionalinformation',
            templateUrl: '/app/user/createparty/partyoptionalinformation/party-optional-information.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], PartyOptionalInformationComponent);
    return PartyOptionalInformationComponent;
}());
exports.PartyOptionalInformationComponent = PartyOptionalInformationComponent;
//# sourceMappingURL=party-optional-information.component.js.map