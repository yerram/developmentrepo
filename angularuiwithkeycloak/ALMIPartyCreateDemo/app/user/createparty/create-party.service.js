"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var keycloak_service_1 = require('./../../keycloak.service');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/observable/throw');
/*
    The createpartyservice calls the backend webservice
    and creates or updates party with the details provided .
    Throws exception if not successfull.
*/
var CreatePartyService = (function () {
    function CreatePartyService(http, ks) {
        this.http = http;
        this.ks = ks;
    }
    CreatePartyService.prototype.createParty = function (party) {
        var _this = this;
        var body = JSON.stringify(party);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this.ks.getToken().then(function (token) {
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer ' + token);
        });
        var options = new http_1.RequestOptions({ headers: headers });
        if (party.partyBasicInformation.partyId) {
            return this.http.put('http://localhost:9000/api/party', body, options)
                .map(function (data) {
                _this.message = data.text();
                return data.ok;
            })
                .catch(this.handleErrorUpdate);
        }
        else {
            return this.http.post('http://localhost:9000/api/party', body, options)
                .map(function (data) {
                _this.message = data.text();
                return data.ok;
            })
                .catch(this.handleErrorCreate);
        }
    };
    CreatePartyService.prototype.handleErrorCreate = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better messag
        return Observable_1.Observable.throw("Party Creation Unsuccessfull");
    };
    CreatePartyService.prototype.handleErrorUpdate = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better messag
        return Observable_1.Observable.throw("Party Updation Unsuccessfull");
    };
    CreatePartyService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, keycloak_service_1.KeycloakService])
    ], CreatePartyService);
    return CreatePartyService;
}());
exports.CreatePartyService = CreatePartyService;
//# sourceMappingURL=create-party.service.js.map