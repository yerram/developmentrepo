import { Injectable } from '@angular/core';
import { Http, HttpModule, Headers, RequestOptions, Response } from '@angular/http';
import { KeycloakService } from './../../keycloak.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
/*
    The maintain-party service calls the backend webservice 
    to fetch all the parties.
*/
@Injectable()
export class MaintainPartyService {
    //https://api.myjson.com/bins/5c2ok
    //http://MNGNET309744D:8080/api/party 
    private  partyUrl  =  'http://localhost:9000/api/party';  // URL to web API
    constructor (private  http:  Http, private ks: KeycloakService)  { }

    maintainPartyDetails() {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.ks.getToken().then(token => {
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer ' + token);
        })

            return  this.http.get(this.partyUrl, headers)
                                .map(this.extractData)
                                .catch(this.handleError);
      }
      private  extractData(res:  Response)  {
            let body  =  res.json();
            return  body  ||  { };
      }
    private handleError(error: any) {
        return Observable.throw("Error");
    }
}
