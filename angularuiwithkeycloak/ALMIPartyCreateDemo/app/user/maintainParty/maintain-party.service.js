"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var keycloak_service_1 = require('./../../keycloak.service');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/observable/throw');
/*
    The maintain-party service calls the backend webservice
    to fetch all the parties.
*/
var MaintainPartyService = (function () {
    function MaintainPartyService(http, ks) {
        this.http = http;
        this.ks = ks;
        //https://api.myjson.com/bins/5c2ok
        //http://MNGNET309744D:8080/api/party 
        this.partyUrl = 'http://localhost:9000/api/party'; // URL to web API
    }
    MaintainPartyService.prototype.maintainPartyDetails = function () {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this.ks.getToken().then(function (token) {
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer ' + token);
        });
        return this.http.get(this.partyUrl, headers)
            .map(this.extractData)
            .catch(this.handleError);
    };
    MaintainPartyService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    MaintainPartyService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw("Error");
    };
    MaintainPartyService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, keycloak_service_1.KeycloakService])
    ], MaintainPartyService);
    return MaintainPartyService;
}());
exports.MaintainPartyService = MaintainPartyService;
//# sourceMappingURL=maintain-party.service.js.map