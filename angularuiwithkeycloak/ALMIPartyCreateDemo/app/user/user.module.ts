import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { UserRouting, userRoutingProviders } from './user.routing';

/*
    The usermodule for loading User specific classes.
*/
@NgModule({
  imports:      [ UserRouting],
  declarations: [ ],
  providers: [userRoutingProviders]
})
export class UserModule { }