"use strict";
var router_1 = require('@angular/router');
var user_home_component_1 = require('./home/user-home.component');
var auth_guard_service_1 = require('../auth-guard.service');
var create_party_component_1 = require('./createparty/create-party.component');
var maintain_party_component_1 = require('./maintainParty/maintain-party.component');
/*
    The userrouting for specifiying the routes which are under User.
*/
var userRoutes = [
    { path: 'userhome', component: user_home_component_1.UserHomeComponent, canActivateChild: [auth_guard_service_1.AuthGuard], children: [
            { path: 'createParty', component: create_party_component_1.CreatePartyComponent },
            { path: 'maintainParty', component: maintain_party_component_1.MaintainPartyComponent }
        ] },
];
exports.userRoutingProviders = [];
exports.UserRouting = router_1.RouterModule.forChild(userRoutes);
//# sourceMappingURL=user.routing.js.map