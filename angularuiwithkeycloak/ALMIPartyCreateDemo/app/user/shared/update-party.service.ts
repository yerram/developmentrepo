import { Injectable } from '@angular/core';
import { Party } from '../createparty/models/party.model';
import { Observable } from 'rxjs/Observable';
import { Http, HttpModule, Headers, RequestOptions, Response } from '@angular/http';
import { KeycloakService } from './../../keycloak.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
/*
    The updateparty service to send Party model from
    maintain-party to createpartycomponent to view Party details
    for updating.
*/

@Injectable()
export class UpdatePartyService {
    party: Party = new Party();
    private partyUrl = 'http://localhost:9000/api/party/';
    constructor(private http: Http, private ks: KeycloakService) { }
    sendParty(party: Party): void {
        this.party = party;
    }
    getParty(): Observable<Party> {
        
        if (this.party.partyBasicInformation.partyId) {
            
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.ks.getToken().then(token => {
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + token);
            });

            return this.http.get(this.partyUrl + this.party.partyBasicInformation.partyId, headers)
                .map(this.extractData)
                .catch(this.handleError);
        }
        return Observable.of(this.party);

    }
    private extractData(res: Response) {
        let  body = <Party>res.json();
        
        return body || { };
    }
    private handleError(error: any) {
        return Observable.throw("Error");
    }
    clearParty(): void {
        this.party = new Party();
    }
}