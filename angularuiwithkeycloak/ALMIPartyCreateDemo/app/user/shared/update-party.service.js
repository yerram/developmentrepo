"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var party_model_1 = require('../createparty/models/party.model');
var Observable_1 = require('rxjs/Observable');
var http_1 = require('@angular/http');
var keycloak_service_1 = require('./../../keycloak.service');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/observable/of');
/*
    The updateparty service to send Party model from
    maintain-party to createpartycomponent to view Party details
    for updating.
*/
var UpdatePartyService = (function () {
    function UpdatePartyService(http, ks) {
        this.http = http;
        this.ks = ks;
        this.party = new party_model_1.Party();
        this.partyUrl = 'http://localhost:9000/api/party/';
    }
    UpdatePartyService.prototype.sendParty = function (party) {
        this.party = party;
    };
    UpdatePartyService.prototype.getParty = function () {
        if (this.party.partyBasicInformation.partyId) {
            var headers = new http_1.Headers();
            headers.append('Content-Type', 'application/json');
            this.ks.getToken().then(function (token) {
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + token);
            });
            return this.http.get(this.partyUrl + this.party.partyBasicInformation.partyId, headers)
                .map(this.extractData)
                .catch(this.handleError);
        }
        return Observable_1.Observable.of(this.party);
    };
    UpdatePartyService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    UpdatePartyService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw("Error");
    };
    UpdatePartyService.prototype.clearParty = function () {
        this.party = new party_model_1.Party();
    };
    UpdatePartyService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, keycloak_service_1.KeycloakService])
    ], UpdatePartyService);
    return UpdatePartyService;
}());
exports.UpdatePartyService = UpdatePartyService;
//# sourceMappingURL=update-party.service.js.map