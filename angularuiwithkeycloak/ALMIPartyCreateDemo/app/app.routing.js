"use strict";
var router_1 = require('@angular/router');
var auth_guard_service_1 = require('./auth-guard.service');
var keycloak_service_1 = require('./keycloak.service');
var user_home_component_1 = require('./user/home/user-home.component');
/*
    The approuting for specifiying the routes for the application.
*/
var appRoutes = [
    { path: '', redirectTo: '/userhome', pathMatch: 'full' },
    { path: 'userhome', canActivate: [auth_guard_service_1.AuthGuard], component: user_home_component_1.UserHomeComponent }
];
exports.appRoutingProviders = [
    auth_guard_service_1.AuthGuard, keycloak_service_1.KeycloakService
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map