import { Injectable } from '@angular/core';
import myGlobals = require('./global');
declare var Keycloak: any;

@Injectable()
export class KeycloakService {
  static auth: any = {};
  static isAdmin:boolean=false;
  static init(): Promise<any> {
    let keycloakAuth: any = new Keycloak('keycloak.json');
    KeycloakService.auth.loggedIn = false;
      return new Promise((resolve, reject) => {
        keycloakAuth.init({ onLoad: 'login-required' })
          .success(() => {
            KeycloakService.auth.loggedIn = true;
            KeycloakService.auth.authz = keycloakAuth;
            KeycloakService.auth.logoutUrl =keycloakAuth.authServerUrl + "/realms/almi/protocol/openid-connect/logout?redirect_uri=http://localhost:3000/index.html" ;
            myGlobals.usr=KeycloakService.auth.authz.tokenParsed.preferred_username;
            KeycloakService.isAdmin=KeycloakService.auth.authz.hasRealmRole("admin");            
            resolve();
          })
          .error(() => {
            reject();
          });
      });
    }

  logout() {
    console.log('*** LOGOUT');
    KeycloakService.auth.loggedIn = false;
     KeycloakService.auth.authz=null;
     
    window.location.href = KeycloakService.auth.logoutUrl;
  }

  getToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      if (KeycloakService.auth.authz.token) {
        KeycloakService.auth.authz.updateToken(5)
          .success(() => {
            resolve(<string>KeycloakService.auth.authz.token);
          })
          .error(() => {
            reject('Failed to refresh token');
          });
      }
    });
  }
}
