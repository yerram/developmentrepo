import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard }      from './auth-guard.service';
import { KeycloakService }      from './keycloak.service';
import { UserHomeComponent } from './user/home/user-home.component';
/*
    The approuting for specifiying the routes for the application.
*/
const appRoutes: Routes = [
    { path: '', redirectTo:'/userhome', pathMatch:'full'},
    { path:'userhome', canActivate: [AuthGuard], component: UserHomeComponent }
];

export const appRoutingProviders: any[] = [
AuthGuard, KeycloakService
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
