"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_module_1 = require('./app.module');
var platform = platform_browser_dynamic_1.platformBrowserDynamic();
platform.bootstrapModule(app_module_1.AppModule);
var keycloak_service_1 = require('./keycloak.service');
keycloak_service_1.KeycloakService.init()
    .then(function () {
    var platform = platform_browser_dynamic_1.platformBrowserDynamic();
    platform.bootstrapModule(app_module_1.AppModule);
})
    .catch(function () { return window.location.reload(); });
//# sourceMappingURL=main.js.map