1. Copy and paste almiTheme folder inside KeyCloak-->Themes.
2. Copy Node_modules and typings from angularui folder to ALMIPartyCreateDemo folder.
3. Start KeyCloak server by navigating to KeyCloak-->Bin and run command "standalone.bat" and configure it.
4. Start angular application by using "npm start".