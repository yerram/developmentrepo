package com.example.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Configuration
@PropertySource(value = "errorcodes.properties")
public class ErrorCodesConfig {
	@Value("${policydetails.notfound}")
	private String policyDetailsNotFound;
	
	@Value("${policydetailsList.notfound}")
	private String policyDetailsListNotFound;

	public String getPolicyDetailsNotFound() {
		return policyDetailsNotFound;
	}

	public void setPolicyDetailsNotFound(String policyDetailsNotFound) {
		this.policyDetailsNotFound = policyDetailsNotFound;
	}

	public String getPolicyDetailsListNotFound() {
		return policyDetailsListNotFound;
	}

	public void setPolicyDetailsListNotFound(String policyDetailsListNotFound) {
		this.policyDetailsListNotFound = policyDetailsListNotFound;
	}
	
	

}
