package com.example.response;

import com.example.model.PolicyDetails;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "PolicyDetailsResponse")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PolicyDetailsResponse {

	private ResponseHeader responseHeader;
	private PolicyDetails policyDetails;
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}
	public void setResponseHeader(ResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}
	public PolicyDetails getPolicyDetails() {
		return policyDetails;
	}
	public void setPolicyDetails(PolicyDetails policyDetails) {
		this.policyDetails = policyDetails;
	}
	
	
	
}
