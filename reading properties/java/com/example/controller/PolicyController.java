package com.example.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.config.ErrorCodesConfig;
import com.example.model.PolicyDetails;
import com.example.service.PolicyService;

@RestController
public class PolicyController {
	@Autowired
	PolicyService _policyService;
	@Autowired
	ErrorCodesConfig errorCodesConfig; 
	
    @RequestMapping(value = "/policysearch/{policyNumber}", produces = "application/json")
    public @ResponseBody ResponseEntity<Object> findPolidyByNumber(@PathVariable Long policyNumber) {
    	PolicyDetails policyDetails = null;
    	policyDetails = _policyService.getPolicyByNumber(policyNumber);
    	if(policyDetails != null) {
    		return new ResponseEntity<Object>(policyDetails, HttpStatus.OK);
    	}
    	else {
    		Map<String,String> responseBody = new HashMap<>();
            responseBody.put("message", errorCodesConfig.getPolicyDetailsNotFound());
            responseBody.put("code", HttpStatus.BAD_REQUEST.value()+"");
            return new ResponseEntity<Object>(responseBody,HttpStatus.BAD_REQUEST);
    	}
    }

    @RequestMapping(value = "/policysearch", produces = "application/json")
    public @ResponseBody ResponseEntity<Object> findAll() {
    	List<PolicyDetails> policyDetailsList = new ArrayList<PolicyDetails>();
    	
    	policyDetailsList.addAll(_policyService.selectAll());
    	if(policyDetailsList.size()>0) {
    		return new ResponseEntity<Object>(policyDetailsList, HttpStatus.OK);
    	} else if(policyDetailsList.size() == 0) {
    		Map<String,String> responseBody = new HashMap<>();
            responseBody.put("message", errorCodesConfig.getPolicyDetailsListNotFound());
            responseBody.put("code", HttpStatus.BAD_REQUEST.value()+"");
            return new ResponseEntity<Object>(responseBody,HttpStatus.NOT_FOUND);
    	}
    	return null;
    }
}
