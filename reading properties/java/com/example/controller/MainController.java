package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.Customer;
import com.example.model.User;

@Controller
public class MainController {

  @RequestMapping("/hello")
  public String option() {
	  System.out.println("IS IN");
	 // model.addAttribute("user",new User());
	  return "option";

  }
  
  @RequestMapping("/userinput")
  public String userInput(Model model) {
	  System.out.println("IS IN");
	  model.addAttribute("user",new User());
	  return "userinput";

  }
  
  @RequestMapping("/delete")
  public String userdelete(Model model) {
	  System.out.println("IS IN");
	  model.addAttribute("user",new User());
	  return "delete";

  }
  
  @RequestMapping("/update")
  public String userupdate(Model model) {
	  System.out.println("IS IN");
	  model.addAttribute("user",new User());
	  return "update";

  }
  
/////////////////////////Customer //////////////////////////
  
  @RequestMapping(value="/customer")
  public String customerOption() {
	 
	  return "customeroption";

  }
  
  @RequestMapping(value="/customerdata")
  public String customerInsert(Model model) {
	  
	  model.addAttribute("customer",new Customer());
	  return "customerdata";

  }
  
  @RequestMapping("/customerfind")
  public String customerFind(Model model){
	  model.addAttribute("customer",new Customer());
	  return "customerfind";
  }
  
  
  @RequestMapping("/customerdelete")
  public String customerDelete(Model model) {
	  model.addAttribute("customer",new Customer());
	  return "customerdelete";

  }
  
  @RequestMapping("/customerupdate")
  public String customerUpdate(Model model) {
	  model.addAttribute("customer",new Customer());
	  return "customerupdate";

  }

}
