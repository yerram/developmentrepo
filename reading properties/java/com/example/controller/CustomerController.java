package com.example.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.model.Customer;
import com.example.service.CustomerService;

@Controller
public class CustomerController {
	@Autowired
	private CustomerService _customerService;

	@RequestMapping(value = "/customerinsert")
	@ResponseBody
	public String createCustomer(String customerName, String loanNo, String policy, String policyStatus,
			@ModelAttribute Customer customer) {

		customer.setCustomerName(customerName);
		customer.setLoanNo(loanNo);
		customer.setPolicy(policy);
		customer.setPolicyStatus(policyStatus);
		try {
			_customerService.save(customer);
		} catch (HibernateException ex) {

		} catch (Exception ex) {

		}
		customer.setCustomerId(customer.getCustomerId());
		return "Customer Added successfully";

	}

	@RequestMapping(value = "/findcustomer")

	public String findCustomerById(Long customerId, @ModelAttribute Customer customer) {

		try {
			Customer customerresult = _customerService.getCustomerById(customerId);
			customer.setCustomerId(customerresult.getCustomerId());
			customer.setCustomerName(customerresult.getCustomerName());
			customer.setLoanNo(customerresult.getLoanNo());
			customer.setPolicy(customerresult.getPolicy());
			customer.setPolicyStatus(customerresult.getPolicyStatus());

		} catch (HibernateException ex) {

		} catch (Exception ex) {

		}
		// if(customer != null){
		// return "customer found successfully" + customer.getCustomerName();
		// }
		// return "customer not found successfully";

		return "customerfindresult";

	}

	@RequestMapping(value = "/customerdeleteid")
	@ResponseBody
	public String delete(Long customerId) {
		try {
			// Customer customer = new Customer(customerId);
			_customerService.delete(customerId);
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return "User succesfully deleted!";
	}

	@RequestMapping(value = "/customerupdatebyid")
	@ResponseBody
	public String update(Long customerId, String customerName, String loanNo, String policy, String policyStatus) {
		try {
			Customer customer = new Customer();
			customer.setCustomerId(customerId);
			customer.setCustomerName(customerName);
			customer.setLoanNo(loanNo);
			customer.setPolicy(policy);
			customer.setPolicyStatus(policyStatus);
			_customerService.update(customer);
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return "User succesfully updated!";
	}

	@RequestMapping(value = "/customerread")
	public String readData(Model model) {
		List<Customer> customerList = new ArrayList<Customer>();
		customerList = _customerService.selectAll();
		model.addAttribute(customerList);
		return "customerread";
	}

}
