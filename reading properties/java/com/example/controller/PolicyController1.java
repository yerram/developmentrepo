package com.example.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.PolicyDetails;
import com.example.service.PolicyService;

@Controller
public class PolicyController1 {
	@Autowired
	PolicyService _policyService;
	
	@RequestMapping(value = "/policySearch/{policyNumber}")
	public String findCustomerById(@PathVariable Long policyNumber, @ModelAttribute PolicyDetails policyDetails) {

		try {
			PolicyDetails policyDetailsResults = _policyService.getPolicyByNumber(policyNumber);
			policyDetails.setPolicyno(policyDetailsResults.getPolicyno());
			policyDetails.setPaymentPlan(policyDetailsResults.getPaymentPlan());
			policyDetails.setStatus(policyDetailsResults.getStatus());
			policyDetails.setTerminationReason(policyDetailsResults.getTerminationReason());
			policyDetails.setTerminationType(policyDetailsResults.getTerminationType());
			policyDetails.setApplicationType(policyDetailsResults.getApplicationType());
			policyDetails.setCoverage(policyDetailsResults.getCoverage());
			policyDetails.setCustomerName(policyDetailsResults.getCustomerName());
			policyDetails.setCustomerNumber(policyDetailsResults.getCustomerNumber());
			policyDetails.setLRV(policyDetailsResults.getLRV());
		} catch (HibernateException ex) {

		} catch (Exception ex) {

		}
		return "policyDetails";

	}
	
	@RequestMapping(value = "/policySearch")
	public String findCustomerById(Model model) {

		try {
			List<PolicyDetails> policyDetailsList = new ArrayList<PolicyDetails>();
			policyDetailsList = _policyService.selectAll();
			model.addAttribute(policyDetailsList);
			return "policyDetailsList";
		} catch (HibernateException ex) {

		} catch (Exception ex) {

		}
		return "policyDetailsList";

	}
}
