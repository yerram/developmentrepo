package com.example.service;

import java.util.List;

import com.example.model.Customer;

public interface CustomerService {

	Customer getCustomerById(Long customerId);

	void save(Customer customer);

	void delete(Long customerId);

	void update(Customer customer);

	public List<Customer> selectAll();
}