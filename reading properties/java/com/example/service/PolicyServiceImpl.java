package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PolicyDao;
import com.example.model.PolicyDetails;

@Service
public class PolicyServiceImpl implements PolicyService{
	
	@Autowired
	private PolicyDao _policyDao;

	@Override
	public PolicyDetails getPolicyByNumber(Long policyNo) {
		return _policyDao.findOne(policyNo);
	}
	@Override
	public List<PolicyDetails> selectAll() {
		return _policyDao.findAll();
	}
}
