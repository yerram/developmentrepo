package com.example.service;

import java.util.List;

import com.example.model.PolicyDetails;

public interface PolicyService {
	
	PolicyDetails getPolicyByNumber(Long policyNo);
	public List<PolicyDetails> selectAll();
}
