package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "policydetails")
public class PolicyDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "policyno")
	private Long policyno;
	
	@NotNull
	@Column(name = "applicationtype")
	private String applicationType;
	
	@NotNull
	@Column(name = "status")
	private String status;
	
	@NotNull
	@Column(name = "paymentplan")
	private String paymentPlan;
	
	@Column(name = "terminationtype")
	private String terminationType;
	
	@Column(name = "lrv")
	private String LRV;
	
	@Column(name = "terminationreason")
	private String terminationReason;
	
	@Column(name = "coverage")
	private String coverage;
	
	@Column(name = "customernumber")
	private String customerNumber;
	
	@Column(name = "customername")
	private String customerName;

	public Long getPolicyno() {
		return policyno;
	}

	public void setPolicyno(Long policyno) {
		this.policyno = policyno;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentPlan() {
		return paymentPlan;
	}

	public void setPaymentPlan(String paymentPlan) {
		this.paymentPlan = paymentPlan;
	}

	public String getTerminationType() {
		return terminationType;
	}

	public void setTerminationType(String terminationType) {
		this.terminationType = terminationType;
	}

	public String getLRV() {
		return LRV;
	}

	public void setLRV(String lRV) {
		LRV = lRV;
	}

	public String getTerminationReason() {
		return terminationReason;
	}

	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
		
}
