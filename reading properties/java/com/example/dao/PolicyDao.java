package com.example.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.PolicyDetails;

@Transactional
public interface PolicyDao extends JpaRepository<PolicyDetails, Long>{
	

}
