/*package com.example.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.model.Customer;

@Repository
@Transactional
public class CustomerDaoImpl implements CustomerDao {
	
	@Autowired
	private SessionFactory _sessionFactory;
	
	
	
	 
	@Override
	public void save(Customer customer){
		_sessionFactory.getCurrentSession().save(customer);
	}
	
	
	@Override
	public void delete(Customer customer) {
		_sessionFactory.getCurrentSession().delete(customer);
		return;
	}
	
	
	 
	@Override
	public void update(Customer customer) {
		_sessionFactory.getCurrentSession().update(customer);
		return;
	}
	
	
	 
	@Override
	public Customer getCustomerById(Long customerId){
		return (Customer)_sessionFactory.getCurrentSession().get(Customer.class, customerId);
	}
	
	
	 
	@Override
	@SuppressWarnings("unchecked")
	public List<Customer> getAll(){
		return _sessionFactory.getCurrentSession().createQuery("from Customer").list();
	}

}
*/