package com.archcap.party.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.archcap.party.entity.CustomerEntity;

@Repository
@Transactional
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

}
