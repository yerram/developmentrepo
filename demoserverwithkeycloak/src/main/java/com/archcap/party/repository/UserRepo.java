package com.archcap.party.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.archcap.party.entity.User;




/**
 * This is the User Repository.
 *
 */
public interface UserRepo extends JpaRepository<User,String> {

}
