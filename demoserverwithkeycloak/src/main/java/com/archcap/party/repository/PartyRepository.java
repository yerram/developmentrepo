package com.archcap.party.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.archcap.party.entity.PartyEntity;

/**
 * This is the Party Repository.
 *
 */
@Repository
@Transactional
public interface PartyRepository extends JpaRepository<PartyEntity, Long> {
	/**
	 * @return returns selective data using the query.
	 */
	@Query("SELECT p.partyId, pbi.partyName, pbi.nickName,pbi.partyType, a.addressState, poi.partyRefId, pbi.partyStatus FROM PartyEntity p INNER JOIN p.partyBasicInformation pbi INNER JOIN p.partyOptionalInformation  poi INNER JOIN p.partyAddress a")
	List<Object[]> queryByRetriveMaintainPartyData();

}
