package com.archcap.party.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.archcap.party.entity.PolicyEntity;

@Repository
@Transactional
public interface PolicyRepository extends JpaRepository<PolicyEntity, Long> {

}
