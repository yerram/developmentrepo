package com.archcap.party.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.archcap.party.bc.PartyBean;

/**
 * This class is used to validate any business logic.
 *
 */
@Component
public class PartyValidator {
	
	/**
	 * @param partyBean
	 */
	public void createPartyValidator(@Validated PartyBean partyBean){
		// To Be Implemented
	}

}
