package com.archcap.party.rest.controller;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.archcap.party.bc.PartyBean;
import com.archcap.party.service.PartyService;
import com.google.gson.Gson;
import com.infy.log.ALMILogging;

/**
 * This class is used to handle the request and response for create,delete,search
 * and update functionalities.It provides rest endpoints for CRUD activities.
 * Cross origin is added to allow access for the REST urls that are sent from client.
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:3000")
public class PartyController {

	@Autowired
	private PartyService partyService;
	
	private final 	Logger slf4jLogger = ALMILogging.getLogger(PartyController.class);

	/**
	 * This method provides the REST endpoint for creating a new party.
	 * @param partyBean RequestBody is used to map the information from
	 * client(in json format) to the bean. Corresponding entity class will be 
	 * used to insert party information to database
	 * @return returns inserted party id. 
	 * http status code on successful insertion.
	 */
	@RequestMapping(value = "/party", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createParty(@RequestBody PartyBean partyBean) {

		Long partyId = partyService.createParty(partyBean);
		Gson gson = new Gson();
		String jsonRequest = gson.toJson(partyBean);
		
		slf4jLogger.info("Request Json : "+jsonRequest);
		slf4jLogger.info("CONTROLLER -> PARTY CREATED SUCCESSFULLY");
		

		return new ResponseEntity<>("PARTY CREATED WITH ID: "+partyId, HttpStatus.CREATED);

	}

	
	/**
	 * This method provides the REST endpoint for getting all the parties available.
	 * @return returns the list of parties which will be
	 * sent to client in json format with http status code.
	 */
	@RequestMapping(value = "/party/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<PartyBean>> readData() {
		List<PartyBean> partyBeanList;
		
		partyBeanList = partyService.readParty();
	
		return new ResponseEntity<>(partyBeanList, HttpStatus.OK);
	}
	
	
	/**
	 * This method provides the REST endpoint for updating a party.
	 * @param partyBean @RequestBody is used to map the information from
	 * client(in json format) to the bean. Corresponding entity class will be 
	 * used to send party information to database
	 * @return returns updated party id.
	 * http status code on successful updation.
	 */
	@RequestMapping(value = "/party", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Object> update(@RequestBody PartyBean partyBean)  {
		
		Long updatedPartyId = partyService.updateParty(partyBean.getPartyBasicInformation().getPartyId(), partyBean);
		return new ResponseEntity<>("PARTY WITH ID "+updatedPartyId+" IS UPDATED", HttpStatus.OK);

	}
	
	/**
	 * This method is used to retrieve a single party based on party id.
	 * @param partyId Id of the party
	 * @return  returns party information of particular party based in party id. 
	 */
	@RequestMapping(value = "/party/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PartyBean> searchPartyById(@PathVariable("id") Long partyId)
			 {
		PartyBean savedPartyBean = partyService.searchPartyById(partyId);
		
		return new ResponseEntity<>(savedPartyBean, HttpStatus.OK);
	}
	
	
	/**
	 * This method provides the REST endpoint for getting all the parties available.
	 * @return returns the list of parties with selective data.
	 * sent to client in json format with http status code.
	 */
	@RequestMapping(value = "/party", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<PartyBean>> readMaintainData() {
		List<PartyBean> partyBeanList;
		
		partyBeanList = partyService.readMaintainPartyData();
		slf4jLogger.info("CONTROLLER -> PARTY DATA RECIEVED SUCCESSFULLY");
	
		return new ResponseEntity<>(partyBeanList, HttpStatus.OK);
	}
	
	
	
	
	

}
