package com.archcap.party.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.archcap.party.entity.User;
import com.archcap.party.repository.UserRepo;
import com.archcap.party.service.AuthenticationService;





/**
 * This class is for userAuthentication.
 *
 */
@RestController
public class UserAuthenticationController {

	@Autowired
	private AuthenticationService service;
	@Autowired
	private  UserRepo user;
	
	
	/**
	 * @param user
	 * @return returns user entity.
	 */
	@RequestMapping(value="/login",
			method=RequestMethod.POST)
	
	public ResponseEntity<User> authenticate(@RequestBody User user){
	if(user!=null){
		boolean flag= service.authenticate(user);
		if(flag){
			return new ResponseEntity<>(user, HttpStatus.OK);
		}
	}
	return new ResponseEntity<>(user, HttpStatus.FORBIDDEN);
		
	}
	
	/**
	 * @return returns user.
	 */
	@RequestMapping(value="/getall",method=RequestMethod.GET)
	public List<User> getEvery(){
		List<User> userList=service.getEvery();
		if(userList.isEmpty())
			return userList;
		return userList;
	}
	
	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/get/{id}",method=RequestMethod.GET)
	public User getEvery(@PathVariable("id") String id){
		
		return user.findOne(id);
	}
}
