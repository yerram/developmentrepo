package com.archcap.party.exception;

import org.springframework.stereotype.Component;

/**
 * This class is used to handle Database Exceptions.
 *
 */
@Component
public class DatabaseExcepiton extends Exception {
	private static final long serialVersionUID = 1L;
	private String errorMessage;

	/**
	 *  Database Exception Constructor.
	 */
	public DatabaseExcepiton() {
		super();
	}

	/**
	 * @param errormessage
	 */
	public DatabaseExcepiton(String errormessage) {
		super(errormessage);
	}

	/**
	 * @return returns message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
}
