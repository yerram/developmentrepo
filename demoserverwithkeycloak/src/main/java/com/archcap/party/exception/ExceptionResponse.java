package com.archcap.party.exception;

import org.springframework.stereotype.Component;

/**
 * This class is use to set error code and description.
 *
 */
@Component
public class ExceptionResponse {

	private int code;
	private String description;
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}



}
