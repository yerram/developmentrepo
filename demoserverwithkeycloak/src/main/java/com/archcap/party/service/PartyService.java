package com.archcap.party.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.archcap.party.bc.PartyBean;

/**
 * This interface is used for service operation.
 *
 */
@Service
public interface PartyService {

	/**
	 * @param partyBean
	 * @return return inserted party id.
	 */
	public Long createParty(PartyBean partyBean);

	/**
	 * @param partyId
	 * @return returns party details based on party id.
	 */
	public PartyBean searchPartyById(Long partyId);

	/**
	 * @param partyId
	 * @param partyBean
	 * @return returns updated party id.
	 */
	public Long updateParty(Long partyId, PartyBean partyBean);

	/**
	 * @return returns list of parties.
	 */
	public List<PartyBean> readParty();
	
	
	/**
	 * @return returns list of parties with selective data.
	 */
	public List<PartyBean> readMaintainPartyData();
	
	
}