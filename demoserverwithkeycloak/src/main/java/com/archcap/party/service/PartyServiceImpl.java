package com.archcap.party.service;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.archcap.party.bc.PartyBean;
import com.archcap.party.dao.PartyDao;
import com.archcap.party.validator.PartyValidator;
import com.infy.log.ALMILogging;

/**
 * This class contains implementation of the partyservice interface
 * which will interact with database to perform the database operations.
 *
 */
@Service
public class PartyServiceImpl implements PartyService {
	
	@Autowired
	private PartyDao partyDao; 
	
	@Autowired
	private PartyValidator partyValidator;
	
	private final 	Logger slf4jLogger = ALMILogging.getLogger(PartyServiceImpl.class);

	/**
	 * This method is used to create a new party 
	 * @param partyBean Contains party information
	 * @return returns created party id.
	 */
	@Override
	public Long createParty(PartyBean partyBean) {
		slf4jLogger.info("CREATE PARTY SERVICE CALLED");
		partyValidator.createPartyValidator(partyBean);
		return partyDao.createParty(partyBean);
	}

	/**
	 * This method is used to search the party based on party id.
	 * @param partyId Id of the party.
	 * @return returns requested party information based on id.
	 */
	@Override
	public PartyBean searchPartyById(Long partyId) {
		return partyDao.searchPartyById(partyId);
	}

	
	
	/**
	 * This method is used to update a party
	 * @param partyId Id of the party
	 * @param partyBean Contains existing party information
	 * @return returns updated party id.
	 */
	@Override
	public Long updateParty(Long partyId, PartyBean partyBean) {
		return partyDao.updateParty(partyId, partyBean);
	}

	/**
	 * This method is used to retrieve all available parties in the database
	 * @return returns list of parties.
	 */
	@Override
	public List<PartyBean> readParty() {
		return partyDao.readParty();
	}

	/**
	 * This method is used to retrieve all available parties in the database
	 * @return returns list of parties with selective data.
	 */
	@Override
	public List<PartyBean> readMaintainPartyData() {
		return partyDao.readMaintainPartyData();
	}

}
