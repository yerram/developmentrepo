package com.archcap.party.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.archcap.party.bc.PolicyBean;
import com.archcap.party.entity.PolicyEntity;

@Service
public interface PolicyService {

	public PolicyEntity createPolicy(PolicyBean policyBean);

	public PolicyEntity searchPolicyById(Long policyNo);

	public void deleteCustomer(Long policyNo);

	public PolicyEntity updatePolicy(Long policyNo, PolicyBean policyBean);

	public List<PolicyEntity> readPolicy();

}