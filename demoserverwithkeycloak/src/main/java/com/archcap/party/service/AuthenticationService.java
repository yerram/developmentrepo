package com.archcap.party.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.archcap.party.entity.User;
import com.archcap.party.repository.UserRepo;



/**
 * This class Authentication Service. 
 *
 */
@Service
public class AuthenticationService {

	@Autowired
	private UserRepo repo;
	
	/**
	 * @param user
	 * @return returns boolean.
	 */
	public boolean authenticate(User user){
		User findUser=repo.findOne(user.getUserName());
		if(findUser==null){
			return false;
		}
		if(findUser.getPassword().equals(user.getPassword())){
			return true;
		}
		return false;
		
	}
	
	/**
	 * @return returns all users.
	 */
	public List<User> getEvery(){
		return repo.findAll();
	}
}
