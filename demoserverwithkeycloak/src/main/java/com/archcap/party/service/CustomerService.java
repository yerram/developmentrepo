package com.archcap.party.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.archcap.party.bc.CustomerBean;
import com.archcap.party.entity.CustomerEntity;

@Service
public interface CustomerService {

	public CustomerEntity createCustomer(CustomerBean customerBean);

	public CustomerEntity searchCustomerById(Long customerId);

	public void deleteCustomer(Long customerId);

	public CustomerEntity updateCustomer(Long customerId, CustomerBean customerBean);

	public List<CustomerEntity> readCustomer();

}