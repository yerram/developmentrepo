package com.archcap.party.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.archcap.party.bc.CustomerBean;
import com.archcap.party.entity.CustomerEntity;

@Repository
@Transactional
public interface CustomerDao {
	public CustomerEntity createCustomer(CustomerBean customer);

	public CustomerEntity searchCustomerById(Long customerId);

	public void deleteCustomer(Long customerId);

	public CustomerEntity updateCustomer(Long customerId, CustomerBean customeBean);

	public List<CustomerEntity> readCustomer();
}