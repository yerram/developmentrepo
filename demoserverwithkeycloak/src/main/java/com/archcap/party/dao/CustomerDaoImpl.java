package com.archcap.party.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.archcap.party.bc.CustomerBean;
import com.archcap.party.bc.PolicyBean;
import com.archcap.party.entity.CustomerEntity;
import com.archcap.party.entity.PolicyEntity;
import com.archcap.party.repository.CustomerRepository;
import com.archcap.party.repository.PolicyRepository;

@Repository
@Transactional
public class CustomerDaoImpl implements CustomerDao {

	@Autowired
	CustomerRepository _customerRepository;

	@Autowired
	PolicyRepository _policyRepository;

	public CustomerEntity createCustomer(CustomerBean customerBean) {

		CustomerEntity customer = new CustomerEntity();
		customer.setCustomerCity(customerBean.getCustomerCity());
		customer.setCustomerName(customerBean.getCustomerName());
		customer.setCustomerNickname(customerBean.getCustomerNickname());
		customer.setCustomerReferenceId(customerBean.getCustomerReferenceId());
		customer.setCustomerState(customerBean.getCustomerState());
		customer.setCustomerType(customerBean.getCustomerType());

		if (customerBean.getPolicy().isEmpty()) {
			System.out.println("policy not specified");
		} else {
			Set<PolicyEntity> policySet = new HashSet<>();

			for (PolicyBean policyBean : customerBean.getPolicy()) {
				if (policyBean.getPolicyNo() != null) {
					PolicyEntity findPolicy = _policyRepository.findOne(policyBean.getPolicyNo());
					if (findPolicy != null) {
						policySet.add(findPolicy);

					} else {
						PolicyEntity policy = new PolicyEntity();
						policy.setApplicationType(policyBean.getApplicationType());
						policy.setCoverage(policyBean.getCoverage());
						policy.setCustomerName(policyBean.getCustomerName());
						policy.setCustomerNumber(policyBean.getCustomerNumber());
						policy.setLRV(policyBean.getLRV());
						policy.setPaymentPlan(policyBean.getPaymentPlan());
						policy.setPolicyNo(policyBean.getPolicyNo());
						policy.setTerminationReason(policy.getTerminationReason());
						policy.setTerminationType(policyBean.getTerminationType());
						policySet.add(policy);
					}

				} else {
					PolicyEntity policy = new PolicyEntity();
					policy.setApplicationType(policyBean.getApplicationType());
					policy.setCoverage(policyBean.getCoverage());
					policy.setCustomerName(policyBean.getCustomerName());
					policy.setCustomerNumber(policyBean.getCustomerNumber());
					policy.setLRV(policyBean.getLRV());
					policy.setPaymentPlan(policyBean.getPaymentPlan());
					policy.setTerminationReason(policy.getTerminationReason());
					policy.setTerminationType(policyBean.getTerminationType());
					policySet.add(policy);
				}
			}

			customer.setPolicy(policySet);
		}

		CustomerEntity customerEntity = _customerRepository.save(customer);
		return customerEntity;

	}

	public CustomerEntity searchCustomerById(Long customerId) {
		CustomerEntity customerEntity = _customerRepository.findOne(customerId);
		return customerEntity;
	}

	public void deleteCustomer(Long customerId) {

		if (searchCustomerById(customerId) != null) {
			_customerRepository.delete(customerId);
		} else {
			// throw exception;
		}

	}

	public CustomerEntity updateCustomer(Long customerId, CustomerBean customerBean) {
		CustomerEntity customerEntity = new CustomerEntity();
		if (searchCustomerById(customerId) != null) {

			customerEntity.setCustomerName(customerBean.getCustomerName());
			customerEntity.setCustomerNickname(customerBean.getCustomerNickname());
			customerEntity.setCustomerCity(customerBean.getCustomerCity());
			customerEntity.setCustomerType(customerBean.getCustomerType());
			customerEntity.setCustomerState(customerBean.getCustomerState());
			customerEntity.setCustomerReferenceId(customerBean.getCustomerReferenceId());
			_customerRepository.save(customerEntity);

		} else {
			// throw error
		}
		return customerEntity;
	}

	public List<CustomerEntity> readCustomer() {
		List<CustomerEntity> customerEntityList = new ArrayList<CustomerEntity>();
		customerEntityList = _customerRepository.findAll();
		return customerEntityList;

	}

}
