package com.archcap.party.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.archcap.party.bc.AddressBean;
import com.archcap.party.bc.PartyBasicInformationBean;
import com.archcap.party.bc.PartyBean;
import com.archcap.party.bc.PartyOptionalInformationBean;
import com.archcap.party.bc.PartyRolesBean;
import com.archcap.party.entity.AddressEntity;
import com.archcap.party.entity.PartyBasicInformatioinEntity;
import com.archcap.party.entity.PartyEntity;
import com.archcap.party.entity.PartyOptionalInformationEntity;
import com.archcap.party.entity.RoleEntity;
import com.archcap.party.repository.PartyRepository;
import com.infy.log.ALMILogging;

/**
 * This is the Customer DAO implementation file for performing 
 * the CRUD operations on the database
 *
 */
@Repository
@Transactional
public class PartyDaoImpl implements PartyDao {

	@Autowired
	private PartyRepository partyRepository;

	private final Logger slf4jLogger = ALMILogging.getLogger(PartyDaoImpl.class);

	/**
	 * This method gets the party information and creates a new party 
	 * using save method of the jpa repository to insert the party information into
	 * the database
	 * @param partyBean Holds the party information
	 * @throws DatabaseException Handles exception on insert
	 * @return returns successfully inserted party id.
	 */
	@Override
	public Long createParty(PartyBean partyBean) {
		slf4jLogger.info("CREATE PARTY DAO CALLED");
		PartyEntity partyEntity = new PartyEntity();
		PartyBasicInformatioinEntity partyBasicInfomation = new PartyBasicInformatioinEntity();
		partyEntity.setPartyBasicInformation(partyBasicInfomation);
		PartyOptionalInformationEntity partyOptionalInformation = new PartyOptionalInformationEntity();
		partyEntity.setPartyOptionalInformation(partyOptionalInformation);

		partyEntity.getPartyBasicInformation().setPartyName(partyBean.getPartyBasicInformation().getPartyName());

		partyEntity.getPartyBasicInformation().setNickName(partyBean.getPartyBasicInformation().getNickName());
		partyEntity.getPartyBasicInformation().setPartyStatus("Pending");
		partyEntity.getPartyBasicInformation().setPartyType(partyBean.getPartyBasicInformation().getPartyType());
		partyEntity.getPartyOptionalInformation()
				.setPartyRefId(partyBean.getPartyOptionalInformation().getPartyRefId());
		partyEntity.getPartyOptionalInformation()
				.setArchLMIRefId(partyBean.getPartyOptionalInformation().getArchLMIRefId());

		Set<RoleEntity> roleEntitySet = new HashSet<>();
		for (String roles : partyBean.getPartyRoles().getPartyRoles()) {
			RoleEntity roleEntity = new RoleEntity();
			roleEntity.setLongName(roles);
			roleEntity.setShortName(roles);
			roleEntitySet.add(roleEntity);
		}

		partyEntity.setPartyRoles(roleEntitySet);

		AddressEntity addressEntity = new AddressEntity();
		addressEntity.setAddressType(partyBean.getAddress().getType());
		addressEntity.setAddressAttn(partyBean.getAddress().getAttn());
		addressEntity.setAddressLine1(partyBean.getAddress().getAddressLine1());
		addressEntity.setAddressLine2(partyBean.getAddress().getAddressLine2());
		addressEntity.setAddressCity(partyBean.getAddress().getCity());
		addressEntity.setAddressState(partyBean.getAddress().getState());
		addressEntity.setAddressPostalCode(partyBean.getAddress().getPostCode());
		addressEntity.setAddressCountry(partyBean.getAddress().getCountry());

		partyEntity.setPartyAddress(addressEntity);

		PartyEntity savedPartyEntity = partyRepository.save(partyEntity);

		return savedPartyEntity.getPartyId();
	}

	/**
	 * This method is used to search a single party based on party id.
	 * @param partyId Id of particular party
	 * @return  returns details of party
	 */
	@Override
	public PartyBean searchPartyById(Long partyId) {

		 PartyEntity partyEntity = partyRepository.findOne(partyId);

		PartyBean savedPartyBean = new PartyBean();
		

		PartyBasicInformationBean partyBasicInformationBean = new PartyBasicInformationBean();
		savedPartyBean.setPartyBasicInformation(partyBasicInformationBean);
		PartyOptionalInformationBean partyOptionalInformationBean = new PartyOptionalInformationBean();
		savedPartyBean.setPartyOptionalInformation(partyOptionalInformationBean);

		savedPartyBean.getPartyBasicInformation().setPartyId(partyEntity.getPartyId());

		savedPartyBean.getPartyBasicInformation().setPartyName(partyEntity.getPartyBasicInformation().getPartyName());
		savedPartyBean.getPartyBasicInformation().setNickName(partyEntity.getPartyBasicInformation().getNickName());
		savedPartyBean.getPartyBasicInformation()
				.setPartyStatus(partyEntity.getPartyBasicInformation().getPartyStatus());
		savedPartyBean.getPartyBasicInformation().setPartyType(partyEntity.getPartyBasicInformation().getPartyType());
		savedPartyBean.getPartyOptionalInformation()
				.setArchLMIRefId(partyEntity.getPartyOptionalInformation().getArchLMIRefId());
		savedPartyBean.getPartyOptionalInformation()
				.setPartyRefId(partyEntity.getPartyOptionalInformation().getPartyRefId());

		PartyRolesBean partyRoleBean = new PartyRolesBean();
		Set<String> partyRoleSet = new HashSet<>();
		for (RoleEntity string : partyEntity.getPartyRoles()) {
			partyRoleSet.add(string.getLongName());
		}
		partyRoleBean.setPartyRoles(partyRoleSet);
		savedPartyBean.setPartyRoles(partyRoleBean);

		AddressBean addressBean = new AddressBean();
		addressBean.setAttn(partyEntity.getPartyAddress().getAddressAttn());
		addressBean.setAddressLine1(partyEntity.getPartyAddress().getAddressLine1());
		addressBean.setAddressLine2(partyEntity.getPartyAddress().getAddressLine2());
		addressBean.setCity(partyEntity.getPartyAddress().getAddressCity());
		addressBean.setCountry(partyEntity.getPartyAddress().getAddressCountry());
		addressBean.setPostCode(partyEntity.getPartyAddress().getAddressPostalCode());
		addressBean.setState(partyEntity.getPartyAddress().getAddressState());
		addressBean.setType(partyEntity.getPartyAddress().getAddressType());

		savedPartyBean.setAddress(addressBean);

		return savedPartyBean;
	}

	

	/**
	 * This method gets the party to be updated using party id and updates the information 
	 * @param partyId Id of the party to be updated.
	 * @param partyBean Details of the party to be updated.
	 * @return returns updated party id.
	 * @throws DatabaseException Handles exception on update
	 */
	@Override
	public Long updateParty(Long partyId, PartyBean partyBean) {
		PartyEntity fetchPartyEntity;

		fetchPartyEntity = partyRepository.findOne(partyId);
		fetchPartyEntity.getPartyBasicInformation().setPartyName(partyBean.getPartyBasicInformation().getPartyName());
		fetchPartyEntity.getPartyBasicInformation().setNickName(partyBean.getPartyBasicInformation().getNickName());
		fetchPartyEntity.getPartyBasicInformation()
				.setPartyStatus(partyBean.getPartyBasicInformation().getPartyStatus());
		fetchPartyEntity.getPartyBasicInformation().setPartyType(partyBean.getPartyBasicInformation().getPartyType());
		fetchPartyEntity.getPartyOptionalInformation()
				.setArchLMIRefId(partyBean.getPartyOptionalInformation().getArchLMIRefId());
		fetchPartyEntity.getPartyOptionalInformation()
				.setPartyRefId(partyBean.getPartyOptionalInformation().getPartyRefId());

		Set<RoleEntity> roleEntitySet = new HashSet<>();
		for (String roles : partyBean.getPartyRoles().getPartyRoles()) {
			RoleEntity roleEntity = new RoleEntity();
			roleEntity.setLongName(roles);
			roleEntity.setShortName(roles);
			roleEntitySet.add(roleEntity);

		}

		fetchPartyEntity.setPartyRoles(roleEntitySet);
		
		
		fetchPartyEntity.getPartyAddress().setAddressType(partyBean.getAddress().getType());
		fetchPartyEntity.getPartyAddress().setAddressAttn(partyBean.getAddress().getAttn());
		fetchPartyEntity.getPartyAddress().setAddressLine1(partyBean.getAddress().getAddressLine1());
		fetchPartyEntity.getPartyAddress().setAddressLine2(partyBean.getAddress().getAddressLine2());
		fetchPartyEntity.getPartyAddress().setAddressCity(partyBean.getAddress().getCity());
		fetchPartyEntity.getPartyAddress().setAddressState(partyBean.getAddress().getState());
		fetchPartyEntity.getPartyAddress().setAddressPostalCode(partyBean.getAddress().getPostCode());
		fetchPartyEntity.getPartyAddress().setAddressCountry(partyBean.getAddress().getCountry());

		return fetchPartyEntity.getPartyId();
	}

	/**
	 * This method gets the list of all parties available.
	 * @return returns list of parties.
	 * @throws DatabaseException Handles exception on reading the data from database.
	 */
	@Override
	public List<PartyBean> readParty() {
		List<PartyBean> partyBeanList = new ArrayList<>();
		List<PartyEntity> partyEntityList = partyRepository.findAll();


		for (PartyEntity partyEntity : partyEntityList) {

			PartyBean savedPartyBean = new PartyBean();

			PartyBasicInformationBean partyBasicInformationBean = new PartyBasicInformationBean();
			savedPartyBean.setPartyBasicInformation(partyBasicInformationBean);
			PartyOptionalInformationBean partyOptionalInformationBean = new PartyOptionalInformationBean();
			savedPartyBean.setPartyOptionalInformation(partyOptionalInformationBean);

			savedPartyBean.getPartyBasicInformation().setPartyId(partyEntity.getPartyId());

			savedPartyBean.getPartyBasicInformation()
					.setPartyName(partyEntity.getPartyBasicInformation().getPartyName());
			savedPartyBean.getPartyBasicInformation().setNickName(partyEntity.getPartyBasicInformation().getNickName());
			savedPartyBean.getPartyBasicInformation()
					.setPartyStatus(partyEntity.getPartyBasicInformation().getPartyStatus());
			savedPartyBean.getPartyBasicInformation()
					.setPartyType(partyEntity.getPartyBasicInformation().getPartyType());
			savedPartyBean.getPartyOptionalInformation()
					.setArchLMIRefId(partyEntity.getPartyOptionalInformation().getArchLMIRefId());
			savedPartyBean.getPartyOptionalInformation()
					.setPartyRefId(partyEntity.getPartyOptionalInformation().getPartyRefId());

			PartyRolesBean partyRoleBean = new PartyRolesBean();
			Set<String> partyRoleSet = new HashSet<>();
			for (RoleEntity string : partyEntity.getPartyRoles()) {
				partyRoleSet.add(string.getLongName());
			}
			partyRoleBean.setPartyRoles(partyRoleSet);
			savedPartyBean.setPartyRoles(partyRoleBean);

			AddressBean addressBean = new AddressBean();
			addressBean.setAttn(partyEntity.getPartyAddress().getAddressAttn());
			addressBean.setAddressLine1(partyEntity.getPartyAddress().getAddressLine1());
			addressBean.setAddressLine2(partyEntity.getPartyAddress().getAddressLine2());
			addressBean.setCity(partyEntity.getPartyAddress().getAddressCity());
			addressBean.setCountry(partyEntity.getPartyAddress().getAddressCity());
			addressBean.setPostCode(partyEntity.getPartyAddress().getAddressPostalCode());
			addressBean.setState(partyEntity.getPartyAddress().getAddressState());
			addressBean.setType(partyEntity.getPartyAddress().getAddressType());

			savedPartyBean.setAddress(addressBean);

			partyBeanList.add(savedPartyBean);

		}

		return partyBeanList;

	}

	/**
	 * This method gets the list of all parties available.
	 * @return returns list of parties with selective data.
	 * @throws DatabaseException Handles exception on reading the data from database.
	 */
	@Override
	public List<PartyBean> readMaintainPartyData() {
		List<PartyBean> partyBeanList = new ArrayList<>();
		List<Object[]>  maintainPartyData = partyRepository.queryByRetriveMaintainPartyData();

		for (Object[] object : maintainPartyData) {
			PartyBean partyBean = new PartyBean();

			PartyBasicInformationBean partyBasicInformationBean = new PartyBasicInformationBean();
			partyBean.setPartyBasicInformation(partyBasicInformationBean);
			PartyOptionalInformationBean partyOptionalInformationBean = new PartyOptionalInformationBean();
			partyBean.setPartyOptionalInformation(partyOptionalInformationBean);
			AddressBean addressBean = new AddressBean();
			partyBean.setAddress(addressBean);

			partyBean.getPartyBasicInformation().setPartyId((Long) object[0]);
			partyBean.getPartyBasicInformation().setPartyName((String) object[1]);
			partyBean.getPartyBasicInformation().setNickName((String) object[2]);
			partyBean.getPartyBasicInformation().setPartyType((String) object[3]);
			partyBean.getAddress().setState((String) object[4]);
			partyBean.getPartyOptionalInformation().setPartyRefId((String) object[5]);
			partyBean.getPartyBasicInformation().setPartyStatus((String) object[6]);

			partyBeanList.add(partyBean);

		}

		return partyBeanList;
	}

}
