package com.archcap.party.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.archcap.party.bc.PolicyBean;
import com.archcap.party.entity.PolicyEntity;

@Repository
@Transactional
public interface PolicyDao {
	public PolicyEntity createPolicy(PolicyBean policyBean);

	public PolicyEntity searchPolicyById(Long policyNo);

	public void deletePolicy(Long policyNo);

	public PolicyEntity updatePolicy(Long policyNo, PolicyBean policyBean);

	public List<PolicyEntity> readPolicy();

}
