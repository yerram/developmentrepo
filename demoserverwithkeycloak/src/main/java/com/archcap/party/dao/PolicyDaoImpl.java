package com.archcap.party.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.archcap.party.bc.PolicyBean;
import com.archcap.party.entity.PolicyEntity;
import com.archcap.party.repository.PolicyRepository;

@Repository
@Transactional
public class PolicyDaoImpl implements PolicyDao {

	PolicyRepository _policyRepository;

	@Override
	public PolicyEntity createPolicy(PolicyBean policyBean) {
		PolicyEntity policyEntity = new PolicyEntity();
		policyEntity.setApplicationType(policyBean.getApplicationType());
		policyEntity.setCoverage(policyBean.getCoverage());
		policyEntity.setCustomerName(policyBean.getCustomerName());
		policyEntity.setCustomerNumber(policyBean.getCustomerNumber());
		policyEntity.setLRV(policyBean.getLRV());
		policyEntity.setPaymentPlan(policyBean.getPaymentPlan());
		policyEntity.setPolicyNo(policyBean.getPolicyNo());
		policyEntity.setTerminationReason(policyBean.getTerminationReason());
		policyEntity.setTerminationType(policyBean.getTerminationType());
		_policyRepository.save(policyEntity);
		return policyEntity;
	}

	@Override
	public PolicyEntity searchPolicyById(Long policyNo) {
		PolicyEntity policyEntity = _policyRepository.findOne(policyNo);
		return policyEntity;
	}

	@Override
	public void deletePolicy(Long policyNo) {
		if (searchPolicyById(policyNo) != null) {
			_policyRepository.delete(policyNo);
		} else {
			// throw exception
		}

	}

	@Override
	public PolicyEntity updatePolicy(Long policyNo, PolicyBean policyBean) {
		PolicyEntity policyEntity = new PolicyEntity();
		if (searchPolicyById(policyNo) != null) {
			policyEntity.setApplicationType(policyBean.getApplicationType());
			policyEntity.setCoverage(policyBean.getCoverage());
			policyEntity.setCustomerName(policyBean.getCustomerName());
			policyEntity.setCustomerNumber(policyBean.getCustomerNumber());
			policyEntity.setLRV(policyBean.getLRV());
			policyEntity.setPaymentPlan(policyBean.getPaymentPlan());
			policyEntity.setStatus(policyBean.getStatus());
			policyEntity.setTerminationReason(policyBean.getTerminationReason());
			policyEntity.setTerminationType(policyBean.getTerminationType());

			_policyRepository.save(policyEntity);

		} else {

		}
		return policyEntity;
	}

	@Override
	public List<PolicyEntity> readPolicy() {
		System.out.println("*************************");
		List<PolicyEntity> policyEntityList= new ArrayList<PolicyEntity>();
		policyEntityList = _policyRepository.findAll();
		
		System.out.println("*****************************************");
		return policyEntityList;
	}

}
