package com.archcap.party.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.archcap.party.bc.PartyBean;

/**
 * This interface is used to define CRUD functionalities.
 *
 */
@Repository
@Transactional
public interface PartyDao {
	
	/**
	 * @param partyBean
	 * @return returns party id
	 */
	public Long createParty(PartyBean partyBean);

	/**
	 * @param partyId
	 * @return returns party details based on party id
	 */
	public PartyBean searchPartyById(Long partyId);

	/**
	 * @param partyId
	 * @param partyBean
	 * @return returns updated party id
	 */
	public Long updateParty(Long partyId, PartyBean partyBean);

	/**
	 * @return return list of parties.
	 */
	public List<PartyBean> readParty();
	
	/**
	 * @return returns specific party details.
	 */
	public List<PartyBean> readMaintainPartyData();

}