package com.archcap.party.bc;

import java.util.Set;


public class CustomerBean {

	private Long customerId;

	
	private String customerName;


	private String customerNickname;


	private String customerType;


	private String customerCity;

	
	private String customerState;

	
	private String customerReferenceId;
	
	
	private Set<PolicyBean> policy;

	public CustomerBean() {

	}

	public CustomerBean(Long customerId) {
		this.setCustomerId(customerId);
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNickname() {
		return customerNickname;
	}

	public void setCustomerNickname(String customerNickname) {
		this.customerNickname = customerNickname;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getCustomerReferenceId() {
		return customerReferenceId;
	}

	public void setCustomerReferenceId(String customerReferenceId) {
		this.customerReferenceId = customerReferenceId;
	}

	public Set<PolicyBean> getPolicy() {
		return policy;
	}

	public void setPolicy(Set<PolicyBean> policy) {
		this.policy = policy;
	}
	
	

}
