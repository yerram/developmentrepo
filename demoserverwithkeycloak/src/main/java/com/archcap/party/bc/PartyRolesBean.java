package com.archcap.party.bc;

import java.util.Set;

/**
 * This class is used to hold party Roles.
 *
 */
public class PartyRolesBean {
	private Set<String> partyRoles;

	/**
	 * @return the partyRoles
	 */
	public Set<String> getPartyRoles() {
		return partyRoles;
	}

	/**
	 * @param partyRoles the partyRoles to set
	 */
	public void setPartyRoles(Set<String> partyRoles) {
		this.partyRoles = partyRoles;
	}



}
