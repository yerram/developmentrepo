package com.archcap.party.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "customer")
public class CustomerEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long customerId;

	@NotNull
	@Column(name = "name")
	private String customerName;

	@NotNull
	@Column(name = "nickname")
	private String customerNickname;

	@Column(name = "type")
	private String customerType;

	@Column(name = "city")
	private String customerCity;

	@Column(name = "state")
	private String customerState;

	@Column(name = "referenceid")
	private String customerReferenceId;
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="customerid", referencedColumnName="id")
	private Set<PolicyEntity> policy;

	public CustomerEntity() {

	}

	public CustomerEntity(Long customerId) {
		this.setCustomerId(customerId);
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNickname() {
		return customerNickname;
	}

	public void setCustomerNickname(String customerNickname) {
		this.customerNickname = customerNickname;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getCustomerReferenceId() {
		return customerReferenceId;
	}

	public void setCustomerReferenceId(String customerReferenceId) {
		this.customerReferenceId = customerReferenceId;
	}

	public Set<PolicyEntity> getPolicy() {
		return policy;
	}

	public void setPolicy(Set<PolicyEntity> policy) {
		this.policy = policy;
	}
	
	

}
