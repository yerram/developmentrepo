package com.archcap.party.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * This class is used map party optional information with database.
 *
 */
@Entity
@Audited
@Table(name = "OPTIONAL_INFORMATION")
public class PartyOptionalInformationEntity {

	@Id
	@Column(name = "OPTIONALINFOID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long optionalInformatinoId;

	@Column(name = "PARTY_REFERENCEID")
	private String partyRefId;
	
	@Column(name = "PARTY_ARCHLMIREFERENCEID")
	private String archLMIRefId;

	/**
	 * @return the optionalInformatinoId
	 */
	public Long getOptionalInformatinoId() {
		return optionalInformatinoId;
	}

	/**
	 * @param optionalInformatinoId the optionalInformatinoId to set
	 */
	public void setOptionalInformatinoId(Long optionalInformatinoId) {
		this.optionalInformatinoId = optionalInformatinoId;
	}

	/**
	 * @return the partyRefId
	 */
	public String getPartyRefId() {
		return partyRefId;
	}

	/**
	 * @param partyRefId the partyRefId to set
	 */
	public void setPartyRefId(String partyRefId) {
		this.partyRefId = partyRefId;
	}

	/**
	 * @return the archLMIRefId
	 */
	public String getArchLMIRefId() {
		return archLMIRefId;
	}

	/**
	 * @param archLMIRefId the archLMIRefId to set
	 */
	public void setArchLMIRefId(String archLMIRefId) {
		this.archLMIRefId = archLMIRefId;
	}



}
