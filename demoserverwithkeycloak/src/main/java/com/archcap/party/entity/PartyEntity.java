package com.archcap.party.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * This class is used map Party Details with database.
 *
 */
@Entity
@Audited
@Table(name = "PARTY")
public class PartyEntity {
	@Id
	@Column(name = "PARTY_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long partyId;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_PARTYINFOID",referencedColumnName="PARTYINFOID", unique = true)
	private PartyBasicInformatioinEntity partyBasicInformation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_OPTIONALINFOID",referencedColumnName="OPTIONALINFOID", unique = true)
	private PartyOptionalInformationEntity partyOptionalInformation;

	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "PARTY_ROLE", joinColumns = { @JoinColumn(name = "PARTY_ID",referencedColumnName="PARTY_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "LONG_NAME", referencedColumnName="LONG_NAME") })
	private Set<RoleEntity> partyRoles;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ADDRESS_ID",referencedColumnName="ADDRESS_ID", unique = true)
	private AddressEntity partyAddress;

	/**
	 * @return the partyId
	 */
	public Long getPartyId() {
		return partyId;
	}

	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	/**
	 * @return the partyBasicInformation
	 */
	public PartyBasicInformatioinEntity getPartyBasicInformation() {
		return partyBasicInformation;
	}

	/**
	 * @param partyBasicInformation the partyBasicInformation to set
	 */
	public void setPartyBasicInformation(PartyBasicInformatioinEntity partyBasicInformation) {
		this.partyBasicInformation = partyBasicInformation;
	}

	/**
	 * @return the partyOptionalInformation
	 */
	public PartyOptionalInformationEntity getPartyOptionalInformation() {
		return partyOptionalInformation;
	}

	/**
	 * @param partyOptionalInformation the partyOptionalInformation to set
	 */
	public void setPartyOptionalInformation(PartyOptionalInformationEntity partyOptionalInformation) {
		this.partyOptionalInformation = partyOptionalInformation;
	}

	/**
	 * @return the partyRoles
	 */
	public Set<RoleEntity> getPartyRoles() {
		return partyRoles;
	}

	/**
	 * @param partyRoles the partyRoles to set
	 */
	public void setPartyRoles(Set<RoleEntity> partyRoles) {
		this.partyRoles = partyRoles;
	}

	/**
	 * @return the partyAddress
	 */
	public AddressEntity getPartyAddress() {
		return partyAddress;
	}

	/**
	 * @param partyAddress the partyAddress to set
	 */
	public void setPartyAddress(AddressEntity partyAddress) {
		this.partyAddress = partyAddress;
	}

	

}
