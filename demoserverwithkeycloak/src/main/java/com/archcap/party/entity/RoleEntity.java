package com.archcap.party.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * This class is used map party roles  with database.
 *
 */
@Entity
@Audited
@Table(name = "ROLES")
public class RoleEntity {

	@Column(name = "SHORT_NAME", nullable = false)
	private String shortName;

	@Id
	@NotEmpty
	@Column(name = "LONG_NAME", nullable = false)
	private String longName;

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the longName
	 */
	public String getLongName() {
		return longName;
	}

	/**
	 * @param longName the longName to set
	 */
	public void setLongName(String longName) {
		this.longName = longName;
	}
	



}
