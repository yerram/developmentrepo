package com.archcap.party.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
/**
 * This class is used map party basic information with database.
 *
 */
@Entity
@Audited
@Table(name = "PARTYINFO")
public class PartyBasicInformatioinEntity {
	
	@Id
	@Column(name="PARTYINFOID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long partyBasicInfoId;
	
	@Column(name = "PARTY_NAME")
	private String partyName;
	
	@Column(name = "PARTY_NICKNAME")
	private String nickName;
	
	@Column(name = "PARTY_STATUS")
	private String partyStatus;
	
	@Column(name = "PARTY_TYPE")
	private String partyType;

	/**
	 * @return the partyBasicInfoId
	 */
	public Long getPartyBasicInfoId() {
		return partyBasicInfoId;
	}

	/**
	 * @param partyBasicInfoId the partyBasicInfoId to set
	 */
	public void setPartyBasicInfoId(Long partyBasicInfoId) {
		this.partyBasicInfoId = partyBasicInfoId;
	}

	/**
	 * @return the partyName
	 */
	public String getPartyName() {
		return partyName;
	}

	/**
	 * @param partyName the partyName to set
	 */
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * @return the partyStatus
	 */
	public String getPartyStatus() {
		return partyStatus;
	}

	/**
	 * @param partyStatus the partyStatus to set
	 */
	public void setPartyStatus(String partyStatus) {
		this.partyStatus = partyStatus;
	}

	/**
	 * @return the partyType
	 */
	public String getPartyType() {
		return partyType;
	}

	/**
	 * @param partyType the partyType to set
	 */
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	


	
	

}
