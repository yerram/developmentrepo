/*package com.example.test;

import javax.persistence.PersistenceContext;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.example.dao.CustomerDao;
import com.example.model.Customer;

@RunWith(SpringRunner.class)
@DataJpaTest
//@Transactional(propagation = Propagation.NOT_SUPPORTED)
@SpringBootTest(classes = CustomerTest.class, webEnvironment=WebEnvironment.RANDOM_PORT)

@AutoConfigureTestDatabase
@EnableConfigurationProperties
@ContextConfiguration(classes = {PersistenceContext.class})
//@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
//        TransactionalTestExecutionListener.class})
@ActiveProfiles("test")



@WebAppConfiguration

@TestExecutionListeners(inheritListeners = false, listeners = {
       DependencyInjectionTestExecutionListener.class,
       DirtiesContextTestExecutionListener.class })



public class CustomerTest extends AbstractTestNGSpringContextTests {
	@Autowired
	private CustomerDao _customerDao;
	
  @Test
  public void test1() {
	  Customer actual = _customerDao.findOne(2L);
	  
	  Customer expected = new Customer();
	  expected.setCustomerId(2L);
	  expected.setCustomerName("abhishek");
	  expected.setLoanNo("no");
	  expected.setPolicy("yes");
	  expected.setPolicyStatus("no");
	  
	  System.out.println("test passed");
	  Assert.assertEquals(actual.getCustomerName(), expected.getCustomerName());
	  
	  
  }
}
*/