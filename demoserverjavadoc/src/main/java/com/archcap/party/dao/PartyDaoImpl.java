package com.archcap.party.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.archcap.party.bc.PartyBean;
import com.archcap.party.entity.AddressEntity;
import com.archcap.party.entity.PartyEntity;
import com.archcap.party.entity.RoleEntity;
import com.archcap.party.exception.DatabaseExcepiton;
import com.archcap.party.repository.PartyRepository;
import com.infy.log.ALMILogging;
@Repository

/**
 * This is the Customer DAO implementation file for performing 
 * the CRUD operations on the database
 *
 */
public class PartyDaoImpl implements PartyDao {
	
	@Autowired
	 private PartyRepository _partyRepository;
	
	private final 	Logger slf4jLogger = ALMILogging.getLogger(PartyDaoImpl.class);

	/**
	 * This method gets the party information and creates a new party 
	 * using save method of the jpa repository to insert the party infomrmation into
	 * the database
	 * @param partyBean Holds the party information
	 * @throws DatabaseException Handles exception on insert
	 * @return returns successfully inserted party with new party id
	 */
	@Override
	public PartyEntity createParty(PartyBean partyBean) throws DatabaseExcepiton {
		slf4jLogger.info("CREATE PARTY DAO CALLED");
		PartyEntity savedPartyEntity;
		try{
		PartyEntity partyEntity = new PartyEntity();
		partyEntity.setPartyName(partyBean.getPartyBasicInformation().getPartyName());
		partyEntity.setPartyNickName(partyBean.getPartyBasicInformation().getNickName());
		partyEntity.setPartyStatus("Pending");
		partyEntity.setPartyType(partyBean.getPartyBasicInformation().getPartyType());
		partyEntity.setPartyReferenceId(partyBean.getPartyOptionalInformation().getPartyRefId());
		partyEntity.setArchLMIReferenceId(partyBean.getPartyOptionalInformation().getArchLMIRefId());
		
		
		Set<RoleEntity> roleEntitySet = new HashSet<RoleEntity>();
		for (String roles : partyBean.getPartyRoles().getPartyRoles()) {
			RoleEntity roleEntity = new RoleEntity();
			roleEntity.setLong_name(roles);
			roleEntity.setShort_name(roles);
			roleEntitySet.add(roleEntity);
			
		}
		
		partyEntity.setPartyRoles(roleEntitySet);
		
		
		
		
		
		AddressEntity addressEntity = new AddressEntity();
		addressEntity.setAddressType(partyBean.getAddress().getType());
		addressEntity.setAddressAttn(partyBean.getAddress().getAttn());
		addressEntity.setAddressLine1(partyBean.getAddress().getAddressLine1());
		addressEntity.setAddressLine2(partyBean.getAddress().getAddressLine2());
		addressEntity.setAddressCity(partyBean.getAddress().getCity());
		addressEntity.setAddressState(partyBean.getAddress().getState());
		addressEntity.setAddressPostalCode(partyBean.getAddress().getPostCode());
		addressEntity.setAddressCountry(partyBean.getAddress().getCountry());
		
		partyEntity.setPartyAddress(addressEntity);
		
		savedPartyEntity= _partyRepository.save(partyEntity);
		
		
			
		}catch(Exception e){
			throw new DatabaseExcepiton("hello");
		}
	
		return savedPartyEntity;
	}

	
	/**
	 * This method is used to search a single party based on party id.
	 * @param partyId Id of particular party
	 * @return  returns details of party
	 */
	@Override
	public PartyEntity searchPartyById(Long partyId)  {
		PartyEntity partyEntity=null;;
		try{
			partyEntity = _partyRepository.findOne(partyId);
		}catch(Exception e){
			
		}
		
		return partyEntity;
	}

	@Override
	public void deleteParty(Long partyId) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * This method gets the party to be updated using party id and updates the information 
	 * @param partyId Id of the party to be updated.
	 * @param partyBean Details of the party to be updated.
	 * @return returns updated information.
	 * @throws DatabaseException Handles exception on update
	 */
	@Override
	public PartyEntity updateParty(Long partyId, PartyBean partyBean) throws DatabaseExcepiton {
		
		PartyEntity fetchPartyEntity;
		try{
		fetchPartyEntity = _partyRepository.findOne(partyId);
		fetchPartyEntity.setPartyName(partyBean.getPartyBasicInformation().getPartyName());
		fetchPartyEntity.setPartyNickName(partyBean.getPartyBasicInformation().getNickName());
		fetchPartyEntity.setPartyStatus(partyBean.getPartyBasicInformation().getPartyStatus());
		fetchPartyEntity.setPartyType(partyBean.getPartyBasicInformation().getPartyType());
		fetchPartyEntity.setArchLMIReferenceId(partyBean.getPartyOptionalInformation().getArchLMIRefId());
		fetchPartyEntity.setPartyReferenceId(partyBean.getPartyOptionalInformation().getPartyRefId());
		
		
		Set<RoleEntity> roleEntitySet = new HashSet<RoleEntity>();
		for (String roles : partyBean.getPartyRoles().getPartyRoles()) {
			RoleEntity roleEntity = new RoleEntity();
			roleEntity.setLong_name(roles);
			roleEntity.setShort_name(roles);
			roleEntitySet.add(roleEntity);
			
		}
		
		fetchPartyEntity.setPartyRoles(roleEntitySet);
		
		AddressEntity addressEntity = new AddressEntity();
		addressEntity.setAddressType(partyBean.getAddress().getType());
		addressEntity.setAddressAttn(partyBean.getAddress().getAttn());
		addressEntity.setAddressLine1(partyBean.getAddress().getAddressLine1());
		addressEntity.setAddressLine2(partyBean.getAddress().getAddressLine2());
		addressEntity.setAddressCity(partyBean.getAddress().getCity());
		addressEntity.setAddressState(partyBean.getAddress().getState());
		addressEntity.setAddressPostalCode(partyBean.getAddress().getPostCode());
		addressEntity.setAddressCountry(partyBean.getAddress().getCountry());
		
		fetchPartyEntity.setPartyAddress(addressEntity);
		}catch(Exception e){
			throw new DatabaseExcepiton();
		}
		
		
		return fetchPartyEntity;
	}
	/**
	 * This method gets the list of all parties available.
	 * @return returns list of parties.
	 * @throws DatabaseException Handles exception on reading the data from database.
	 */
	@Override
	public List<PartyEntity> readParty()throws DatabaseExcepiton {
		List<PartyEntity> partyEntityList = new ArrayList<PartyEntity>();
		try{
		 partyEntityList= _partyRepository.findAll();
		}catch(Exception e){
			throw new DatabaseExcepiton();
		}
		// TODO Auto-generated method stub
		return partyEntityList;
	}

}
