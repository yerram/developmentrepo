package com.archcap.party.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.archcap.party.entity.User;
import com.archcap.party.repository.UserRepo;
import com.archcap.party.service.AuthenticationService;



@RestController
public class UserAuthenticationController {

	@Autowired
	private AuthenticationService _service;
	@Autowired
	private  UserRepo u;
	
	@RequestMapping(value="/login",
			method=RequestMethod.POST)
	
	public ResponseEntity<User> authenticate(@RequestBody User user){
	if(user!=null){
		boolean b= _service.authenticate(user);
		if(b){
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
	}
	return new ResponseEntity<User>(user, HttpStatus.FORBIDDEN);
		
	}
	@RequestMapping(value="/getall",method=RequestMethod.GET)
	public List<User> getEvery(){
		List<User> u=_service.getEvery();
		if(u.isEmpty())return null;
		return u;
	}
	
	@RequestMapping(value="/get/{id}",method=RequestMethod.GET)
	public User getEvery(@PathVariable("id") String id){
		
		return u.findOne(id);
	}
}
