package com.archcap.party.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.archcap.party.exception.CustomException;
import com.archcap.party.exception.DatabaseExcepiton;
import com.archcap.party.exception.ExceptionResponse;
import com.archcap.party.exception.PolicyNotFoundException;

@ControllerAdvice
public class ErrorHandlingController {
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ExceptionResponse> generalException(Exception e)throws Exception{
		ExceptionResponse exceptionResponse = new ExceptionResponse();
		exceptionResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		exceptionResponse.setDescription(e.getMessage());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(CustomException.class)
	public ResponseEntity<ExceptionResponse> customException(CustomException ce)throws Exception{
		ExceptionResponse exceptionResponse = new ExceptionResponse();
		exceptionResponse.setCode(HttpStatus.BAD_REQUEST.value());
		exceptionResponse.setDescription(ce.getMessage());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(PolicyNotFoundException.class)
	public ResponseEntity<ExceptionResponse> policyNotFoundException(Exception e)throws Exception{
		ExceptionResponse exceptionResponse = new ExceptionResponse();
		exceptionResponse.setCode(HttpStatus.PRECONDITION_FAILED.value());
		exceptionResponse.setDescription(e.getMessage());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.OK);
	}
	
	@ExceptionHandler(DatabaseExcepiton.class)
	public ResponseEntity<ExceptionResponse> databaseExcepiton(Exception e)throws Exception{
		ExceptionResponse exceptionResponse = new ExceptionResponse();
		exceptionResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		exceptionResponse.setDescription(e.getMessage());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
