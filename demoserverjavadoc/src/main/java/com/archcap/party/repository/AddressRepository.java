package com.archcap.party.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.archcap.party.entity.AddressEntity;

/**
 * This is address repository
 *
 */
@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Long> {

}
