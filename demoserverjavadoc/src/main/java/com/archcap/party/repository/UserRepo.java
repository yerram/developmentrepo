package com.archcap.party.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.archcap.party.entity.User;




/**
 * this is user repository
 */
public interface UserRepo extends JpaRepository<User,String> {

}
