package com.archcap.party.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.archcap.party.entity.User;
import com.archcap.party.repository.UserRepo;



@Service
public class AuthenticationService {

	@Autowired
	private UserRepo _repo;
	
	public boolean authenticate(User user){
		User findUser=_repo.findOne(user.getUser_name());
		if(findUser==null){
			return false;
		}
		if(findUser.getPassword().equals(user.getPassword())){
			return true;
		}
		return false;
		
	}
	
	public List<User> getEvery(){
		System.out.println(_repo.findAll());
		return _repo.findAll();
	}
}
