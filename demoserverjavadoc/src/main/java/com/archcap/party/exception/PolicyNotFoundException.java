package com.archcap.party.exception;

public class PolicyNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public PolicyNotFoundException() {
		super();
	}

	public PolicyNotFoundException(String errormessage) {
		super(errormessage);
	}

}
