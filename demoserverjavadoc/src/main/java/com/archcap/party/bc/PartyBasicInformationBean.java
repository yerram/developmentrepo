package com.archcap.party.bc;

/**
 * This class holds the basic information of the party.
 *
 */
public class PartyBasicInformationBean {

	private Long partyId;
	private String partyName;
	private String nickName;
	private String partyStatus;
	private String partyType;
	/**
	 * @return the partyId
	 */
	public Long getPartyId() {
		return partyId;
	}
	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}
	/**
	 * @return the partyName
	 */
	public String getPartyName() {
		return partyName;
	}
	/**
	 * @param partyName the partyName to set
	 */
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}
	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	/**
	 * @return the partyStatus
	 */
	public String getPartyStatus() {
		return partyStatus;
	}
	/**
	 * @param partyStatus the partyStatus to set
	 */
	public void setPartyStatus(String partyStatus) {
		this.partyStatus = partyStatus;
	}
	/**
	 * @return the partyType
	 */
	public String getPartyType() {
		return partyType;
	}
	/**
	 * @param partyType the partyType to set
	 */
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}
	
	



}
