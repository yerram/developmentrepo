package com.archcap.party.bc;

/**
 * 
 * This class is address bean for holding address information.
 *
 */
public class AddressBean {
	private String attn;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String postCode;
	private String country;
	private String type;



	/**
	 * This returns the type of this address
	 * @return type of the address
	 */
	public String getType() {
		return type;
	}

	/**
	 * This sets the type of the address
	 * @param type of the address
	 */
	public void setType(String type) {
		this.type = type;
	}



	/**
	 * This returns the attention of the addresss
	 * @return attention of the address
	 */
	public String getAttn() {
		return attn;
	}

	/**
	 * This sets the attention of the address
	 * @param attention of the address
	 */
	public void setAttn(String attn) {
		this.attn = attn;
	}

	/**
	 * This returns address line 1 of the address
	 * @return  address line 1 of the address
	 */
	public String getAddressLine1() {
		return addressLine1;
	}
	/**
	 * This sets the address line 1 of the address
	 * @param address line 1 of the address
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * This returns the the address line 2 of the address
	 * @return the address line 2 of the address
	 */
	public String getAddressLine2() {
		return addressLine2;
	}
	/**
	 * This sets the the address line 2 of the address
	 * @param the address line 2 of the address
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	/**
	 * This returns the city of this address
	 * @return type of the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * This sets the city of the address
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * This returns the state of this address
	 * @return state of the address
	 */
	public String getState() {
		return state;
	}
	/**
	 * This sets the state of the address
	 * @param state of the address
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * This returns the postal code of this address
	 * @return  postal code of this address
	 */
	public String getPostCode() {
		return postCode;
	}
	/**
	 * This sets the postal code of this address
	 * @param  postal code of this address
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	/**
	 * This returns the country of the address
	 * @return country of the address
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * This sets the country of the address
	 * @param country of the address
	 */
	public void setCountry(String country) {
		this.country = country;
	}

}
