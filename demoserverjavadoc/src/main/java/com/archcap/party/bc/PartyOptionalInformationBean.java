package com.archcap.party.bc;

/**
 * This class holds the optional information of the party.
 *
 */
public class PartyOptionalInformationBean {
	private String partyRefId;
	private String archLMIRefId;
	/**
	 * @return the partyRefId
	 */
	public String getPartyRefId() {
		return partyRefId;
	}
	/**
	 * @param partyRefId the partyRefId to set
	 */
	public void setPartyRefId(String partyRefId) {
		this.partyRefId = partyRefId;
	}
	/**
	 * @return the archLMIRefId
	 */
	public String getArchLMIRefId() {
		return archLMIRefId;
	}
	/**
	 * @param archLMIRefId the archLMIRefId to set
	 */
	public void setArchLMIRefId(String archLMIRefId) {
		this.archLMIRefId = archLMIRefId;
	}



}
