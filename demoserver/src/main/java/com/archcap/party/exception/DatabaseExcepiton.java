package com.archcap.party.exception;

import org.springframework.stereotype.Component;

@Component
public class DatabaseExcepiton extends Exception {
	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public DatabaseExcepiton() {
		super();
	}

	public DatabaseExcepiton(String errormessage) {
		super(errormessage);
	}
}
