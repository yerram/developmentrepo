package com.archcap.party.exception;

public class ExceptionThrower {

	public void throwGeneralException() throws Exception {
		Exception exception = new Exception("Error for General Exception");
		throw exception;

	}

	public void customerNotFoundException() throws CustomException {
		CustomException customException = new CustomException();
		customException.setCode(10);
		customException.setMessage("Customer not found");
		throw customException;
	}

}
