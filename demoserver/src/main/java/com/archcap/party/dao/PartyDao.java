package com.archcap.party.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.archcap.party.bc.PartyBean;
import com.archcap.party.entity.PartyEntity;
import com.archcap.party.exception.DatabaseExcepiton;
/**
 * This is the Party DAO interface which will be implemented to perform
 * CRUD operations on the database.
 *
 */
@Repository
@Transactional
public interface PartyDao {
	
	public PartyEntity createParty(PartyBean partyBean)throws DatabaseExcepiton;

	public PartyEntity searchPartyById(Long partyId);

	public void deleteParty(Long partyId);

	public PartyEntity updateParty(Long partyId, PartyBean partyBean)throws DatabaseExcepiton;

	public List<PartyEntity> readParty() throws DatabaseExcepiton;

}