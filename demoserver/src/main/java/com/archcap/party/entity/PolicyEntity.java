package com.archcap.party.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "policy")
public class PolicyEntity {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long policyNo;

	@Column(name = "applicationtype")
	private String applicationType;

	@Column(name = "status")
	private String status;

	@Column(name = "paymentplan")
	private String paymentPlan;

	@Column(name = "terminationtype")
	private String terminationType;

	@Column(name = "lrv")
	private String LRV;

	@Column(name = "terminationreason")
	private String terminationReason;

	@Column(name = "coverage")
	private String coverage;

	@Column(name = "number")
	private String customerNumber;

	@Column(name = "name")
	private String customerName;

	public PolicyEntity() {

	}

	public PolicyEntity(Long policyNo, String applicationType, String status, String paymentPlan) {
		this.policyNo = policyNo;
		this.applicationType = applicationType;
		this.status = status;
		this.paymentPlan = paymentPlan;
	}

	public Long getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(Long policyNo) {
		this.policyNo = policyNo;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentPlan() {
		return paymentPlan;
	}

	public void setPaymentPlan(String paymentPlan) {
		this.paymentPlan = paymentPlan;
	}

	public String getTerminationType() {
		return terminationType;
	}

	public void setTerminationType(String terminationType) {
		this.terminationType = terminationType;
	}

	public String getLRV() {
		return LRV;
	}

	public void setLRV(String lRV) {
		LRV = lRV;
	}

	public String getTerminationReason() {
		return terminationReason;
	}

	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	
	
	


}
