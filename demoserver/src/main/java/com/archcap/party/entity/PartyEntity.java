package com.archcap.party.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
/**
 * 
 * This class is used to hold party information
 * and will be used for database operations.
 *
 */
@Entity
@Table(name = "PARTY")
public class PartyEntity {
	@Id
	@Column(name = "PARTY_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long partyId;
	
	@NotNull
	@Column(name = "PARTY_NAME")
	private String partyName;

	@NotNull
	@Column(name = "PARTY_NICKNAME")
	private String partyNickName;

	@Column(name = "PARTY_STATUS")
	private String partyStatus;

	@NotNull
	@Column(name = "PARTY_TYPE")
	private String partyType;

	@Column(name = "PARTY_REFERENCEID")
	private String partyReferenceId;

	@Column(name = "PARTY_ARCHLMIREFERENCEID")
	private String archLMIReferenceId;

	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="PARTY_ID", referencedColumnName="PARTY_ID")
	private Set<RoleEntity> partyRoles;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "READDRESS_ID",referencedColumnName="ADDRESS_ID", unique = true)
	private AddressEntity partyAddress;

	/**
	 * @return the partyId
	 */
	public Long getPartyId() {
		return partyId;
	}

	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	/**
	 * @return the partyName
	 */
	public String getPartyName() {
		return partyName;
	}

	/**
	 * @param partyName the partyName to set
	 */
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	/**
	 * @return the partyNickName
	 */
	public String getPartyNickName() {
		return partyNickName;
	}

	/**
	 * @param partyNickName the partyNickName to set
	 */
	public void setPartyNickName(String partyNickName) {
		this.partyNickName = partyNickName;
	}

	/**
	 * @return the partyStatus
	 */
	public String getPartyStatus() {
		return partyStatus;
	}

	/**
	 * @param partyStatus the partyStatus to set
	 */
	public void setPartyStatus(String partyStatus) {
		this.partyStatus = partyStatus;
	}

	/**
	 * @return the partyType
	 */
	public String getPartyType() {
		return partyType;
	}

	/**
	 * @param partyType the partyType to set
	 */
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	/**
	 * @return the partyReferenceId
	 */
	public String getPartyReferenceId() {
		return partyReferenceId;
	}

	/**
	 * @param partyReferenceId the partyReferenceId to set
	 */
	public void setPartyReferenceId(String partyReferenceId) {
		this.partyReferenceId = partyReferenceId;
	}

	/**
	 * @return the archLMIReferenceId
	 */
	public String getArchLMIReferenceId() {
		return archLMIReferenceId;
	}

	/**
	 * @param archLMIReferenceId the archLMIReferenceId to set
	 */
	public void setArchLMIReferenceId(String archLMIReferenceId) {
		this.archLMIReferenceId = archLMIReferenceId;
	}

	/**
	 * @return the partyRoles
	 */
	public Set<RoleEntity> getPartyRoles() {
		return partyRoles;
	}

	/**
	 * @param partyRoles the partyRoles to set
	 */
	public void setPartyRoles(Set<RoleEntity> partyRoles) {
		this.partyRoles = partyRoles;
	}

	/**
	 * @return the partyAddress
	 */
	public AddressEntity getPartyAddress() {
		return partyAddress;
	}

	/**
	 * @param partyAddress the partyAddress to set
	 */
	public void setPartyAddress(AddressEntity partyAddress) {
		this.partyAddress = partyAddress;
	}

	

}
