package com.archcap.party.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;
/**
 * 
 * This class is used to hold address information
 * and will be used for database operations.
 *
 */
@Entity
@Table(name = "ROLES")
public class RoleEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ROLE_TYPE_ID")
	private Long role_type_id;

	@NotEmpty
	@Column(name = "SHORT_NAME", nullable = false)
	private String short_name;

	
	@Column(name = "LONG_NAME", nullable = false)
	private String long_name;


	/**
	 * @return the role_type_id
	 */
	public Long getRole_type_id() {
		return role_type_id;
	}


	/**
	 * @param role_type_id the role_type_id to set
	 */
	public void setRole_type_id(Long role_type_id) {
		this.role_type_id = role_type_id;
	}


	/**
	 * @return the short_name
	 */
	public String getShort_name() {
		return short_name;
	}


	/**
	 * @param short_name the short_name to set
	 */
	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}


	/**
	 * @return the long_name
	 */
	public String getLong_name() {
		return long_name;
	}


	/**
	 * @param long_name the long_name to set
	 */
	public void setLong_name(String long_name) {
		this.long_name = long_name;
	}



}
