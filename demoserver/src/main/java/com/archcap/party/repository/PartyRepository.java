package com.archcap.party.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.archcap.party.entity.PartyEntity;
/**
 * This interface is used to define CRUD operations  
 */
@Repository
@Transactional
public interface PartyRepository extends JpaRepository<PartyEntity, Long> {

}
