package com.archcap.party.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.archcap.party.bc.PolicyBean;
import com.archcap.party.dao.PolicyDao;
import com.archcap.party.entity.PolicyEntity;

@Service
public class PolicyServiceImpl implements PolicyService {
	@Autowired
	PolicyDao _policyDao;

	@Override
	public PolicyEntity createPolicy(PolicyBean policyBean) {
		PolicyEntity policyEntity = _policyDao.createPolicy(policyBean);
		return policyEntity;
	}

	@Override
	public PolicyEntity searchPolicyById(Long policyNo) {
		PolicyEntity policyEntity = _policyDao.searchPolicyById(policyNo);
		return policyEntity;
	}

	@Override
	public void deleteCustomer(Long policyNo) {
		_policyDao.deletePolicy(policyNo);

	}

	@Override
	public PolicyEntity updatePolicy(Long policyNo, PolicyBean policyBean) {
		PolicyEntity policyEntity = _policyDao.updatePolicy(policyNo, policyBean);
		return policyEntity;
	}

	@Override
	public List<PolicyEntity> readPolicy() {
		System.out.println("----------------");
		List<PolicyEntity> policyEntityList = _policyDao.readPolicy();
		System.out.println("---------------------------------");
		return policyEntityList;
	}

}
