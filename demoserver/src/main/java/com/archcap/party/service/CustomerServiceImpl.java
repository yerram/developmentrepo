package com.archcap.party.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.archcap.party.bc.CustomerBean;
import com.archcap.party.dao.CustomerDao;
import com.archcap.party.entity.CustomerEntity;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao _customerDao;

	@Override
	public CustomerEntity createCustomer(CustomerBean customerBean) {
		CustomerEntity customerEntity = _customerDao.createCustomer(customerBean);
		return customerEntity;

	}

	@Override
	public CustomerEntity searchCustomerById(Long customerId) {
		CustomerEntity customerEntity = _customerDao.searchCustomerById(customerId);
		return customerEntity;

	}

	@Override
	public void deleteCustomer(Long customerId) {

		_customerDao.deleteCustomer(customerId);

	}

	@Override
	public CustomerEntity updateCustomer(Long customerId, CustomerBean customerBean) {
		CustomerEntity customerEntity = _customerDao.updateCustomer(customerId, customerBean);

		return customerEntity;
	}

	@Override
	public List<CustomerEntity> readCustomer() {
		List<CustomerEntity> customerEntityList = _customerDao.readCustomer();
		return customerEntityList;
	}

}
