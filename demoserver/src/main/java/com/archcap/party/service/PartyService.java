package com.archcap.party.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.archcap.party.bc.PartyBean;
import com.archcap.party.entity.PartyEntity;
import com.archcap.party.exception.DatabaseExcepiton;

/**
 * This interface is used to define the methods
 * that will be implemented to process create,
 * search,update and read functionalities.
 *
 */
@Service
public interface PartyService {

	public PartyEntity createParty(PartyBean partyBean)throws DatabaseExcepiton;

	public PartyEntity searchPartyById(Long partyId);

	public void deleteParty(Long partyId);

	public PartyEntity updateParty(Long partyId, PartyBean partyBean)throws DatabaseExcepiton;

	public List<PartyEntity> readParty()throws DatabaseExcepiton;

}