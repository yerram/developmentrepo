package com.archcap.party.service;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.archcap.party.bc.PartyBean;
import com.archcap.party.dao.PartyDao;
import com.archcap.party.entity.PartyEntity;
import com.archcap.party.exception.DatabaseExcepiton;
import com.infy.log.ALMILogging;

/**
 * This class contains implementation of the partyservice interface
 * which will interact with database to perform the database operations.
 *
 */
@Service
public class PartyServiceImpl implements PartyService {
	
	@Autowired
	private PartyDao _partyDao; 
	
	private final 	Logger slf4jLogger = ALMILogging.getLogger(PartyServiceImpl.class);

	/**
	 * This method is used to create a new party 
	 * @param partyBean Contains party information
	 * @return returns party information with created party id.
	 */
	@Override
	public PartyEntity createParty(PartyBean partyBean) throws DatabaseExcepiton {
		slf4jLogger.info("CREATE PARTY SERVICE CALLED");
		PartyEntity partyEntity = _partyDao.createParty(partyBean);
		return partyEntity;
	}

	/**
	 * This method is used to search the party based on party id.
	 * @param partyId Id of the party.
	 * @return returns requested party information based on id.
	 */
	@Override
	public PartyEntity searchPartyById(Long partyId) {
		PartyEntity partyEntity = _partyDao.searchPartyById(partyId);
		return partyEntity;
	}

	@Override
	public void deleteParty(Long partyId) {
		// TODO Auto-generated method stub
		
	}

	
	/**
	 * This method is used to update a party
	 * @param partyId Id of the party
	 * @param partyBean Contains existing party information
	 * @return returns updated party information.
	 */
	@Override
	public PartyEntity updateParty(Long partyId, PartyBean partyBean) throws DatabaseExcepiton {
		PartyEntity partyEntity = _partyDao.updateParty(partyId, partyBean);
		return partyEntity;
	}

	/**
	 * This method is used to retrieve all available parties in the database
	 * @return returns list of parties.
	 */
	@Override
	public List<PartyEntity> readParty() throws DatabaseExcepiton {
		List<PartyEntity> partyEntity = _partyDao.readParty();
		// TODO Auto-generated method stub
		return partyEntity;
	}

}
