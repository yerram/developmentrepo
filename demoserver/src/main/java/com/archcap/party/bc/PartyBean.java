package com.archcap.party.bc;
/**
 * This class is used for holding party information.
 *
 */
public class PartyBean {

	private PartyBasicInformationBean partyBasicInformation;
	private PartyOptionalInformationBean partyOptionalInformation;
	private PartyRolesBean partyRoles;
	private AddressBean address;
	/**
	 * @return the partyBasicInformation
	 */
	public PartyBasicInformationBean getPartyBasicInformation() {
		return partyBasicInformation;
	}
	/**
	 * @param partyBasicInformation the partyBasicInformation to set
	 */
	public void setPartyBasicInformation(PartyBasicInformationBean partyBasicInformation) {
		this.partyBasicInformation = partyBasicInformation;
	}
	/**
	 * @return the partyOptionalInformation
	 */
	public PartyOptionalInformationBean getPartyOptionalInformation() {
		return partyOptionalInformation;
	}
	/**
	 * @param partyOptionalInformation the partyOptionalInformation to set
	 */
	public void setPartyOptionalInformation(PartyOptionalInformationBean partyOptionalInformation) {
		this.partyOptionalInformation = partyOptionalInformation;
	}
	/**
	 * @return the partyRoles
	 */
	public PartyRolesBean getPartyRoles() {
		return partyRoles;
	}
	/**
	 * @param partyRoles the partyRoles to set
	 */
	public void setPartyRoles(PartyRolesBean partyRoles) {
		this.partyRoles = partyRoles;
	}
	/**
	 * @return the address
	 */
	public AddressBean getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(AddressBean address) {
		this.address = address;
	}

	

}
