package com.archcap.party.bc;
/**
 * 
 * This class holds the party roles
 *
 */
import java.util.Set;

public class PartyRolesBean {
	private Set<String> partyRoles;

	/**
	 * @return the partyRoles
	 */
	public Set<String> getPartyRoles() {
		return partyRoles;
	}

	/**
	 * @param partyRoles the partyRoles to set
	 */
	public void setPartyRoles(Set<String> partyRoles) {
		this.partyRoles = partyRoles;
	}

	

}
