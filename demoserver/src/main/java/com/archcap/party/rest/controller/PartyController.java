package com.archcap.party.rest.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.archcap.party.bc.AddressBean;
import com.archcap.party.bc.PartyBasicInformationBean;
import com.archcap.party.bc.PartyBean;
import com.archcap.party.bc.PartyOptionalInformationBean;
import com.archcap.party.bc.PartyRolesBean;
import com.archcap.party.entity.PartyEntity;
import com.archcap.party.entity.RoleEntity;
import com.archcap.party.exception.DatabaseExcepiton;
import com.archcap.party.service.PartyService;
import com.google.gson.Gson;
import com.infy.log.ALMILogging;
/**
 * This class is used to handle the request and response for create,delete,search
 * and update functionalities.It provides rest endpoints for CRUD activities.
 * Cross origin is added to allow access for the REST urls that are sent from client.
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:3000")
public class PartyController {
	
	private final 	Logger slf4jLogger = ALMILogging.getLogger(PartyController.class);
	
	@Autowired
	private PartyService _partyService;
	
	/**
	 * This method provides the REST endpoint for creating a new party.
	 * @param partyBean RequestBody is used to map the information from
	 * client(in json format) to the bean. Corresponding entity class will be 
	 * used to insert party information to database
	 * @return returns inserted party in json format with 
	 * http status code on successful insertion.
	 */
	@RequestMapping(value = "/party", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> createParty(@RequestBody PartyBean partyBean){

		PartyEntity partyEntity;
		PartyBean savedPartyBean=null;
		try {
			
			partyEntity = _partyService.createParty(partyBean);
	
		
		savedPartyBean = new PartyBean();
		
		PartyBasicInformationBean partyBasicInformationBean = new PartyBasicInformationBean();
		savedPartyBean.setPartyBasicInformation(partyBasicInformationBean);
		PartyOptionalInformationBean partyOptionalInformationBean = new PartyOptionalInformationBean();
		savedPartyBean.setPartyOptionalInformation(partyOptionalInformationBean);
		
		savedPartyBean.getPartyBasicInformation().setPartyId(partyEntity.getPartyId());
		
		savedPartyBean.getPartyBasicInformation().setPartyName(partyEntity.getPartyName());
		savedPartyBean.getPartyBasicInformation().setNickName(partyEntity.getPartyNickName());
		savedPartyBean.getPartyBasicInformation().setPartyStatus(partyEntity.getPartyStatus());
		savedPartyBean.getPartyBasicInformation().setPartyType(partyEntity.getPartyType());
		savedPartyBean.getPartyOptionalInformation().setArchLMIRefId(partyEntity.getArchLMIReferenceId());
		savedPartyBean.getPartyOptionalInformation().setPartyRefId(partyEntity.getPartyReferenceId());
		
		PartyRolesBean partyRoleBean = new PartyRolesBean();
		Set<String> partyRoleSet = new HashSet<>();
		for (RoleEntity string : partyEntity.getPartyRoles()) {
			partyRoleSet.add(string.getLong_name());
		}
		partyRoleBean.setPartyRoles(partyRoleSet);
		savedPartyBean.setPartyRoles(partyRoleBean);
		
		AddressBean addressBean = new AddressBean();
		addressBean.setAttn(partyEntity.getPartyAddress().getAddressAttn());
		addressBean.setAddressLine1(partyEntity.getPartyAddress().getAddressLine1());
		addressBean.setAddressLine2(partyEntity.getPartyAddress().getAddressLine2());
		addressBean.setCity(partyEntity.getPartyAddress().getAddressCity());
		addressBean.setCountry(partyEntity.getPartyAddress().getAddressCity());
		addressBean.setPostCode(partyEntity.getPartyAddress().getAddressPostalCode());
		addressBean.setState(partyEntity.getPartyAddress().getAddressState());
		addressBean.setType(partyEntity.getPartyAddress().getAddressType());
		
		savedPartyBean.setAddress(addressBean);
		
		Gson gson = new Gson();
		String jsonRequest = gson.toJson(partyBean);
		
		slf4jLogger.info("Request Json : "+jsonRequest);
		slf4jLogger.info("CONTROLLER -> PARTY CREATED SUCCESSFULLY");
		
		
		} catch (DatabaseExcepiton e) {
			System.out.println("Handle Exception");
		}
		
		

		return new ResponseEntity<Object>("Party Created successfully with id : "+ savedPartyBean.getPartyBasicInformation().getPartyId(), HttpStatus.CREATED);

	}
	
	/**
	 * This method provides the REST endpoint for getting all the parties available.
	 * @return returns the list of parties which will be
	 * sent to client in json format with http status code.
	 */
	@RequestMapping(value = "/party", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<PartyBean>> readData() {
		List<PartyEntity> partyEntityList;
		List<PartyBean> partyBeanList=null;
		try {
			partyEntityList = _partyService.readParty();
	
		
		 partyBeanList= new ArrayList<PartyBean>();
		
		for (PartyEntity partyEntity : partyEntityList) {
			
			PartyBean savedPartyBean = new PartyBean();
			
			PartyBasicInformationBean partyBasicInformationBean = new PartyBasicInformationBean();
			savedPartyBean.setPartyBasicInformation(partyBasicInformationBean);
			PartyOptionalInformationBean partyOptionalInformationBean = new PartyOptionalInformationBean();
			savedPartyBean.setPartyOptionalInformation(partyOptionalInformationBean);
			
			savedPartyBean.getPartyBasicInformation().setPartyId(partyEntity.getPartyId());
			
			savedPartyBean.getPartyBasicInformation().setPartyName(partyEntity.getPartyName());
			savedPartyBean.getPartyBasicInformation().setNickName(partyEntity.getPartyNickName());
			savedPartyBean.getPartyBasicInformation().setPartyStatus(partyEntity.getPartyStatus());
			savedPartyBean.getPartyBasicInformation().setPartyType(partyEntity.getPartyType());
			savedPartyBean.getPartyOptionalInformation().setArchLMIRefId(partyEntity.getArchLMIReferenceId());
			savedPartyBean.getPartyOptionalInformation().setPartyRefId(partyEntity.getPartyReferenceId());
			
			PartyRolesBean partyRoleBean = new PartyRolesBean();
			Set<String> partyRoleSet = new HashSet<>();
			for (RoleEntity string : partyEntity.getPartyRoles()) {
				partyRoleSet.add(string.getLong_name());
			}
			partyRoleBean.setPartyRoles(partyRoleSet);
			savedPartyBean.setPartyRoles(partyRoleBean);
			
			AddressBean addressBean = new AddressBean();
			addressBean.setAttn(partyEntity.getPartyAddress().getAddressAttn());
			addressBean.setAddressLine1(partyEntity.getPartyAddress().getAddressLine1());
			addressBean.setAddressLine2(partyEntity.getPartyAddress().getAddressLine2());
			addressBean.setCity(partyEntity.getPartyAddress().getAddressCity());
			addressBean.setCountry(partyEntity.getPartyAddress().getAddressCountry());
			addressBean.setPostCode(partyEntity.getPartyAddress().getAddressPostalCode());
			addressBean.setState(partyEntity.getPartyAddress().getAddressState());
			addressBean.setType(partyEntity.getPartyAddress().getAddressType());
			
			savedPartyBean.setAddress(addressBean);
			
			partyBeanList.add(savedPartyBean);
			
		}
		} catch (DatabaseExcepiton e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ResponseEntity<Collection<PartyBean>>(partyBeanList, HttpStatus.OK);
	}
	
	/**
	 * This method provides the REST endpoint for updating a party.
	 * @param partyBean @RequestBody is used to map the information from
	 * client(in json format) to the bean. Corresponding entity class will be 
	 * used to send party information to database
	 * @return returns updated party in json format with 
	 * http status code on successful updation.
	 */
	@RequestMapping(value = "/party/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Object> update(@PathVariable("id") Long partyId, @RequestBody PartyBean partyBean)  {
		PartyEntity partyEntity;
		PartyBean savedPartyBean=null;
		try {
			partyEntity = _partyService.updateParty(partyId, partyBean);
		
		
			savedPartyBean = new PartyBean();
		
		PartyBasicInformationBean partyBasicInformationBean = new PartyBasicInformationBean();
		savedPartyBean.setPartyBasicInformation(partyBasicInformationBean);
		PartyOptionalInformationBean partyOptionalInformationBean = new PartyOptionalInformationBean();
		savedPartyBean.setPartyOptionalInformation(partyOptionalInformationBean);
		
		savedPartyBean.getPartyBasicInformation().setPartyId(partyEntity.getPartyId());
		
		savedPartyBean.getPartyBasicInformation().setPartyName(partyEntity.getPartyName());
		savedPartyBean.getPartyBasicInformation().setNickName(partyEntity.getPartyNickName());
		savedPartyBean.getPartyBasicInformation().setPartyStatus(partyEntity.getPartyStatus());
		savedPartyBean.getPartyBasicInformation().setPartyType(partyEntity.getPartyType());
		savedPartyBean.getPartyOptionalInformation().setArchLMIRefId(partyEntity.getArchLMIReferenceId());
		savedPartyBean.getPartyOptionalInformation().setPartyRefId(partyEntity.getPartyReferenceId());
		
		PartyRolesBean partyRoleBean = new PartyRolesBean();
		Set<String> partyRoleSet = new HashSet<>();
		for (RoleEntity string : partyEntity.getPartyRoles()) {
			partyRoleSet.add(string.getLong_name());
		}
		partyRoleBean.setPartyRoles(partyRoleSet);
		savedPartyBean.setPartyRoles(partyRoleBean);
		
		AddressBean addressBean = new AddressBean();
		addressBean.setAttn(partyEntity.getPartyAddress().getAddressAttn());
		addressBean.setAddressLine1(partyEntity.getPartyAddress().getAddressLine1());
		addressBean.setAddressLine2(partyEntity.getPartyAddress().getAddressLine2());
		addressBean.setCity(partyEntity.getPartyAddress().getAddressCity());
		addressBean.setCountry(partyEntity.getPartyAddress().getAddressCity());
		addressBean.setPostCode(partyEntity.getPartyAddress().getAddressPostalCode());
		addressBean.setState(partyEntity.getPartyAddress().getAddressState());
		addressBean.setType(partyEntity.getPartyAddress().getAddressType());
		
		savedPartyBean.setAddress(addressBean);
		} catch (DatabaseExcepiton e) {
			System.out.println("handle exception");
		}
		return new ResponseEntity<Object>("PARTY UPDATED SUCCESSFULLY", HttpStatus.OK);

	}
	
	/**
	 * This method is used to retrieve a single party based on party id.
	 * @param partyId Id of the party
	 * @return  returns party information of particular party based in party id. 
	 */
	@RequestMapping(value = "/party/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PartyBean> searchPartyById(@PathVariable("id") Long partyId)
			 {

		PartyEntity partyEntity = _partyService.searchPartyById(partyId);
		
		PartyBean savedPartyBean = new PartyBean();
		
		PartyBasicInformationBean partyBasicInformationBean = new PartyBasicInformationBean();
		savedPartyBean.setPartyBasicInformation(partyBasicInformationBean);
		PartyOptionalInformationBean partyOptionalInformationBean = new PartyOptionalInformationBean();
		savedPartyBean.setPartyOptionalInformation(partyOptionalInformationBean);
		
		savedPartyBean.getPartyBasicInformation().setPartyId(partyEntity.getPartyId());
		
		savedPartyBean.getPartyBasicInformation().setPartyName(partyEntity.getPartyName());
		savedPartyBean.getPartyBasicInformation().setNickName(partyEntity.getPartyNickName());
		savedPartyBean.getPartyBasicInformation().setPartyStatus(partyEntity.getPartyStatus());
		savedPartyBean.getPartyBasicInformation().setPartyType(partyEntity.getPartyType());
		savedPartyBean.getPartyOptionalInformation().setArchLMIRefId(partyEntity.getArchLMIReferenceId());
		savedPartyBean.getPartyOptionalInformation().setPartyRefId(partyEntity.getPartyReferenceId());
		
		PartyRolesBean partyRoleBean = new PartyRolesBean();
		Set<String> partyRoleSet = new HashSet<>();
		for (RoleEntity string : partyEntity.getPartyRoles()) {
			partyRoleSet.add(string.getLong_name());
		}
		partyRoleBean.setPartyRoles(partyRoleSet);
		savedPartyBean.setPartyRoles(partyRoleBean);
		
		AddressBean addressBean = new AddressBean();
		addressBean.setAttn(partyEntity.getPartyAddress().getAddressAttn());
		addressBean.setAddressLine1(partyEntity.getPartyAddress().getAddressLine1());
		addressBean.setAddressLine2(partyEntity.getPartyAddress().getAddressLine2());
		addressBean.setCity(partyEntity.getPartyAddress().getAddressCity());
		addressBean.setCountry(partyEntity.getPartyAddress().getAddressCity());
		addressBean.setPostCode(partyEntity.getPartyAddress().getAddressPostalCode());
		addressBean.setState(partyEntity.getPartyAddress().getAddressState());
		addressBean.setType(partyEntity.getPartyAddress().getAddressType());
		
		savedPartyBean.setAddress(addressBean);

		return new ResponseEntity<PartyBean>(savedPartyBean, HttpStatus.OK);
	}
	
	
	
	
	
	
	

}
