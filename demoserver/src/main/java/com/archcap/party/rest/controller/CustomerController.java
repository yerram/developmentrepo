package com.archcap.party.rest.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.archcap.party.bc.CustomerBean;
import com.archcap.party.entity.CustomerEntity;
import com.archcap.party.exception.CustomException;
import com.archcap.party.service.CustomerService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:3000")
public class CustomerController {
	// public final Logger slf4j = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CustomerService _customerService;

	@RequestMapping(value = "/customer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CustomerEntity> createCustomer(@RequestBody CustomerBean customerBean) {

		CustomerEntity customerEntity = _customerService.createCustomer(customerBean);

		return new ResponseEntity<CustomerEntity>(customerEntity, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CustomerEntity> searchCustomerById(@PathVariable("id") Long customerId)
			throws CustomException {

		CustomerEntity customerEntity = _customerService.searchCustomerById(customerId);

		return new ResponseEntity<CustomerEntity>(customerEntity, HttpStatus.OK);
	}

	@RequestMapping(value = "/customer/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long customerId) {

		_customerService.deleteCustomer(customerId);
		return new ResponseEntity<Object>("Deleted with customer id weith " + customerId + " successfully",
				HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/customer/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Object> update(@PathVariable("id") Long customerId, @RequestBody CustomerBean customer) {
		CustomerEntity customerEntity = _customerService.updateCustomer(customerId, customer);
		return new ResponseEntity<Object>(customerEntity, HttpStatus.OK);

	}

	@RequestMapping(value = "/customer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<CustomerEntity>> readData() {
		List<CustomerEntity> customerEntityList = _customerService.readCustomer();
		return new ResponseEntity<Collection<CustomerEntity>>(customerEntityList, HttpStatus.OK);
	}

}
