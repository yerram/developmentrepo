package com.archcap.party.rest.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.archcap.party.bc.PolicyBean;
import com.archcap.party.entity.PolicyEntity;
import com.archcap.party.service.PolicyService;

@RestController
@RequestMapping("/api")
public class PolicyController {

	@Autowired
	PolicyService _policyService;

	@RequestMapping(value = "/policy/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> searchPolicyById(@PathVariable("id") Long policyNo) {
		PolicyEntity policyEntity = _policyService.searchPolicyById(policyNo);
		return new ResponseEntity<Object>(policyEntity, HttpStatus.FOUND);

	}

	@RequestMapping(value = "/policy", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<PolicyEntity>> readData() {
		System.out.println("//////////////");
		List<PolicyEntity> policyEntityList = _policyService.readPolicy();
		System.out.println("//////////////");
		return new ResponseEntity<Collection<PolicyEntity>>(policyEntityList, HttpStatus.OK);
	}

	@RequestMapping(value = "/policy/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") Long policyNo) {
		_policyService.deleteCustomer(policyNo);
		return new ResponseEntity<Object>("Deleted with policy  with no " + policyNo + " successfully",
				HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/policy/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> update(@PathVariable("id") Long policyNo, @RequestBody PolicyBean policyBean) {
		PolicyEntity policyEntity = _policyService.updatePolicy(policyNo, policyBean);
		return new ResponseEntity<Object>(policyEntity, HttpStatus.OK);
	}
	
	@RequestMapping(value="/policy", method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PolicyEntity> createPolicy(@RequestBody PolicyBean policyBean){
		PolicyEntity policyEntity = _policyService.createPolicy(policyBean);
		return new ResponseEntity<PolicyEntity>(policyEntity,HttpStatus.CREATED);
		
	}

}
